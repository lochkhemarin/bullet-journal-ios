// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let crown = ImageAsset(name: "crown")
  internal static let eventCompleted = ImageAsset(name: "event_completed")
  internal static let eventEmpty = ImageAsset(name: "event_empty")
  internal static let note = ImageAsset(name: "note")
  internal static let star = ImageAsset(name: "star")
  internal static let taskCompleted = ImageAsset(name: "task_completed")
  internal static let taskEmpty = ImageAsset(name: "task_empty")
  internal static let crownIndicator = ImageAsset(name: "crown_indicator")
  internal static let day1 = ImageAsset(name: "day_1")
  internal static let day2 = ImageAsset(name: "day_2")
  internal static let day3 = ImageAsset(name: "day_3")
  internal static let eventIndicator = ImageAsset(name: "event_indicator")
  internal static let starIndicator = ImageAsset(name: "star_indicator")
  internal static let taskIndicator = ImageAsset(name: "task_indicator")
  internal static let todayHighlight = ImageAsset(name: "today_highlight")
  internal static let backgroundColor = ColorAsset(name: "backgroundColor")
  internal static let errorColor = ColorAsset(name: "errorColor")
  internal static let onBackgroundColor = ColorAsset(name: "onBackgroundColor")
  internal static let onErrorColor = ColorAsset(name: "onErrorColor")
  internal static let onPrimaryColor = ColorAsset(name: "onPrimaryColor")
  internal static let onSecondaryColor = ColorAsset(name: "onSecondaryColor")
  internal static let onSurfaceColor = ColorAsset(name: "onSurfaceColor")
  internal static let primaryColor = ColorAsset(name: "primaryColor")
  internal static let primaryVariantColor = ColorAsset(name: "primaryVariantColor")
  internal static let secondaryColor = ColorAsset(name: "secondaryColor")
  internal static let secondaryVariantColor = ColorAsset(name: "secondaryVariantColor")
  internal static let surfaceColor = ColorAsset(name: "surfaceColor")
  internal static let apple = ImageAsset(name: "apple")
  internal static let balloon = ImageAsset(name: "balloon")
  internal static let balloon2 = ImageAsset(name: "balloon2")
  internal static let balloon3 = ImageAsset(name: "balloon3")
  internal static let bird1 = ImageAsset(name: "bird1")
  internal static let bird2 = ImageAsset(name: "bird2")
  internal static let bird3 = ImageAsset(name: "bird3")
  internal static let bird4 = ImageAsset(name: "bird4")
  internal static let bird5 = ImageAsset(name: "bird5")
  internal static let bird6 = ImageAsset(name: "bird6")
  internal static let bird7 = ImageAsset(name: "bird7")
  internal static let bunny = ImageAsset(name: "bunny")
  internal static let bunny2 = ImageAsset(name: "bunny2")
  internal static let bunny3 = ImageAsset(name: "bunny3")
  internal static let crewCut = ImageAsset(name: "crew cut")
  internal static let deer = ImageAsset(name: "deer")
  internal static let dog = ImageAsset(name: "dog")
  internal static let duck = ImageAsset(name: "duck")
  internal static let elephant = ImageAsset(name: "elephant")
  internal static let elephant2 = ImageAsset(name: "elephant2")
  internal static let fox = ImageAsset(name: "fox")
  internal static let fox2 = ImageAsset(name: "fox2")
  internal static let giraffe = ImageAsset(name: "giraffe")
  internal static let hippo = ImageAsset(name: "hippo")
  internal static let kitty = ImageAsset(name: "kitty")
  internal static let lion = ImageAsset(name: "lion")
  internal static let mouse = ImageAsset(name: "mouse")
  internal static let mouse2 = ImageAsset(name: "mouse2")
  internal static let penguin = ImageAsset(name: "penguin")
  internal static let teddy = ImageAsset(name: "teddy")
  internal static let teddy2 = ImageAsset(name: "teddy2")
  internal static let tortoise = ImageAsset(name: "tortoise")
  internal static let whale = ImageAsset(name: "whale")
  internal static let month1 = ImageAsset(name: "month_1")
  internal static let month10 = ImageAsset(name: "month_10")
  internal static let month11 = ImageAsset(name: "month_11")
  internal static let month12 = ImageAsset(name: "month_12")
  internal static let month2 = ImageAsset(name: "month_2")
  internal static let month3 = ImageAsset(name: "month_3")
  internal static let month4 = ImageAsset(name: "month_4")
  internal static let month5 = ImageAsset(name: "month_5")
  internal static let month6 = ImageAsset(name: "month_6")
  internal static let month7 = ImageAsset(name: "month_7")
  internal static let month8 = ImageAsset(name: "month_8")
  internal static let month9 = ImageAsset(name: "month_9")
  internal static let home = ImageAsset(name: "home")
  internal static let i = ImageAsset(name: "i")
  internal static let shadow = ImageAsset(name: "shadow")
  internal static let store = ImageAsset(name: "store")
  internal static let tabbar = ImageAsset(name: "tabbar")
  internal static let tracker = ImageAsset(name: "tracker")
  internal static let calendarTracker = ImageAsset(name: "calendar_tracker")
  internal static let list = ImageAsset(name: "list")
  internal static let quote = ImageAsset(name: "quote")
  internal static let verticalTracker = ImageAsset(name: "vertical_tracker")
  internal static let background = ImageAsset(name: "background")
  internal static let doneTracker = ImageAsset(name: "done_tracker")
  internal static let drag = ImageAsset(name: "drag")
  internal static let headerLine = ImageAsset(name: "header_line")
  internal static let remove = ImageAsset(name: "remove")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct DataAsset {
  internal fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  internal var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
