//
//  AppCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class AppCoordinator: BaseCoordinator {

    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let theme = BujoTheme.shared

    var window: UIWindow
    var coordinators = [Screen: Coordinator]()

    init(window: UIWindow) {
        self.window = window
    }

    override func start() {
        let viewController = storyboard.instantiateViewController(withIdentifier: Screen.tabbar.storyboardID)
        let date = Date()

        if let tabbarController = viewController as? UITabBarController {
            // Home
            let homeNavigation = UINavigationController()
            let homeCoordinator = HomeSceneCoordinator(tabBar: tabbarController,
                                                             navigationController: homeNavigation,
                                                             initialDate: date)
            addChildCoordinator(homeCoordinator)
            homeCoordinator.start()

            // Tracker
            let trackerNavigation = UINavigationController()
            let trackerCoordinator = TrackerSceneCoordinator(tabBar: tabbarController,
                                                             navigationController: trackerNavigation,
                                                             initialDate: date)
            addChildCoordinator(trackerCoordinator)
            trackerCoordinator.start()

            // Help
            let helpNavigation = UINavigationController()
            let helpCoordinator = HelpCoordinator(navigationController: helpNavigation)
            addChildCoordinator(helpCoordinator)
            helpCoordinator.start()
            tabbarController.viewControllers = [homeNavigation, trackerNavigation, helpNavigation]
        }

        UITabBar.appearance().tintColor = theme.color.primary

        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
}
