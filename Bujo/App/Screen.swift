//
//  Screen.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

/// List screens using in the app
enum Screen: String {
    case tabbar
    case home
    case tracker
    case doodle
    case help

    var storyboardID: String {
        return "\(self.rawValue)_controller"
    }
}
