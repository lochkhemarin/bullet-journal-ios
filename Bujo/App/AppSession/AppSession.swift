//
//  AppSession.swift
//  Bujo
//
//  Created by Khemarin on 2/11/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class AppSession {
    var month: Month

    /// This is the instance in AppDelegate
    static var session: AppSession {
        let sharedAppDelegate = UIApplication.shared.delegate as? AppDelegate
        if let appDelegate = sharedAppDelegate {
            return appDelegate.appSession
        } else {
            let session = AppSession()
            sharedAppDelegate?.appSession = session
            return session
        }
    }

    init() {
        month = Date().month ?? Month(year: 2019, month: 1)
    }

}
