//
//  BigCalendarDayOfWeekCell.swift
//  Bujo
//
//  Created by Khemarin on 11/4/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class BigCalendarDayOfWeekCell: UICollectionViewCell {

    @IBOutlet weak var dayOfWeekLabel: UILabel!
    let theme = BujoTheme.shared

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dayOfWeekLabel.font = theme.font.body2
        dayOfWeekLabel.textColor = theme.color.onBackground
    }

}
