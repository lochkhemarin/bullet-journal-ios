//
//  BigCalendarView.swift
//  Bujo
//
//  Created by Khemarin on 10/16/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

protocol BigCalendarViewDelegate: class {
    func dateClick(in calendarView: BigCalendarView, day: Int)
}

class BigCalendarView: UIView, UICollectionViewDataSource,
        UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    weak var delegate: BigCalendarViewDelegate?

    var cellBorderUtils = Int.random(in: 0...4)

    private var cellWidth: CGFloat = 0.0
    private var cellHeight: CGFloat = 0.0
    private let sectionTop: CGFloat = 5.0
    private let sectionBottom: CGFloat = 5.0
    private let dayOfWeekHeight: CGFloat = 50.0

    private var dateLeftAlignment: CGFloat = 0.0
    private var dateTopAlignment: CGFloat = 0.0

    private var currentDay: Int = 0
    var selectedDay: Int = 0

    var weekdaySymbols: [String] = []
    var month: Month = Date().month {
        didSet {
            if let days = CalendarGrid(baseMonth: month).grid {
                updateCurrentDay()
                daysOfTheMonth = days
            }
        }
    }
    private var daysOfTheMonth: [[Int]] {
        didSet {
            calculateCellSize()
            collectionView.reloadData()
        }
    }
    var collectionView: UICollectionView
    var indicators: [Int: [TodoListViewModel.ListType]] = [:] {
        didSet {
            collectionView.reloadData()
        }
    }

    override init(frame: CGRect) {
        let layout = UICollectionViewFlowLayout()
        let bounds = CGRect(origin: .zero, size: frame.size)
        collectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        collectionView.clipsToBounds = false
        collectionView.backgroundColor = UIColor.clear
        collectionView.allowsSelection = true
        collectionView.allowsMultipleSelection = false
        collectionView.isMultipleTouchEnabled = false
        daysOfTheMonth = []

        super.init(frame: frame)
        addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        calculateCellSize()
        self.clipsToBounds = false

        // Register nib
        let dayCellNib = UINib(nibName: BigCalendarCollectionViewCell.nibName, bundle: nil)
        collectionView.register(dayCellNib,
                                forCellWithReuseIdentifier: BigCalendarCollectionViewCell.cellIdentifier)
        let daySymbolCellNib = UINib(nibName: BigCalendarDayOfWeekCell.nibName, bundle: nil)
        collectionView.register(daySymbolCellNib,
                                forCellWithReuseIdentifier: BigCalendarDayOfWeekCell.cellIdentifier)
        updateCurrentDay()
    }

    private func updateCurrentDay() {
        // get current day number
        if month == Date().month {
            let date = Date()
            let calendar = Calendar.current
            currentDay = calendar.component(.day, from: date)
        } else {
            currentDay = 0
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func calculateCellSize() {
        let totalWeeks: CGFloat = CGFloat(daysOfTheMonth.count)
        if totalWeeks != 0 {
            cellWidth = (frame.width - (sectionTop + sectionBottom) * 6) / 7
            cellHeight = (frame.height - dayOfWeekHeight -
            ((sectionBottom + sectionTop) * (totalWeeks + 1))) / totalWeeks
            dateLeftAlignment = cellWidth / 4
            dateTopAlignment = cellHeight / 4
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return daysOfTheMonth.count + 1
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BigCalendarDayOfWeekCell.cellIdentifier,
                                                          for: indexPath)
            if let caledarDayOfWeekCell = cell as? BigCalendarDayOfWeekCell {
                caledarDayOfWeekCell.dayOfWeekLabel.text = weekdaySymbols[indexPath.item]
            }
            return cell

        } else {
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier:BigCalendarCollectionViewCell.cellIdentifier, for: indexPath)
            if let calendarCell = cell as? BigCalendarCollectionViewCell {
                calendarCell.dateLabel.text = "\(daysOfTheMonth[indexPath.section - 1][indexPath.item])"
                calendarCell.leftLayoutConstraint.constant = dateLeftAlignment
                calendarCell.topLayoutConstraint.constant = dateTopAlignment

                let day = daysOfTheMonth[indexPath.section - 1][indexPath.item]

                if day == currentDay {
                    calendarCell.todayImageView.isHidden = false
                }

                if day == selectedDay {
                    calendarCell.updateSelectedUI(true)
                } else {
                    calendarCell.updateSelectedUI(false)
                }

                calendarCell.borderImageView.image = UIImage(named: "day_\(((cellBorderUtils + indexPath.item) % 3) + 1)")
                // indicators
                if let dayIndicators = indicators[day], !dayIndicators.isEmpty {
                    calendarCell.renderIndicators(dayIndicators)
                }

            }

            // hide cell no date
            if daysOfTheMonth.count > indexPath.section - 1,
                daysOfTheMonth[indexPath.section - 1].count > indexPath.item {
                if daysOfTheMonth[indexPath.section - 1][indexPath.item] == 0 {
                    cell.contentView.isHidden = true
                }
            }

            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: cellWidth, height: dayOfWeekHeight)
        } else {
            return CGSize(width: cellWidth, height: cellHeight)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: sectionTop, left: 0.0, bottom: sectionBottom, right: 0.0)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section > 0 {
            if daysOfTheMonth[indexPath.section - 1][indexPath.item] > 0 {
                let day = (daysOfTheMonth[indexPath.section - 1][indexPath.item])
                selectedDay = day
                collectionView.reloadData()
                delegate?.dateClick(in: self, day: day)
            }
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
