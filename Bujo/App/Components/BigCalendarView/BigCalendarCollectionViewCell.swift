//
//  BigCalendarCollectionViewCell.swift
//  Bujo
//
//  Created by Khemarin on 10/16/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class BigCalendarCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var todayImageView: UIImageView!
    @IBOutlet weak var borderImageView: UIImageView!

    @IBOutlet weak var leftLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var indicatorIcons: [UIImageView]!

    let theme = BujoTheme.shared

    override func awakeFromNib() {
        super.awakeFromNib()

        for indicator in indicatorIcons {
            indicator.isHidden = true
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        todayImageView.isHidden = true
        contentView.isHidden = false

        for indicator in indicatorIcons {
            indicator.isHidden = true
        }
    }

    func renderIndicators(_ indicators: [TodoListViewModel.ListType]) {
        var types: [Int] = []
        var signifers: [Int] = []

        func appendIfNotExist(array: [Int], value: Int?, increment: Int = 0) -> [Int] {
            var result = array
            guard let value = value else {
                return array
            }
            let afterIncrement = value + increment
            if array.contains(afterIncrement) {
                return array
            } else {
                result.append(afterIncrement)
                return result
            }
        }

        for type in indicators {
            switch type {
            case .event(state: _, signifier: let signifier):
                types = appendIfNotExist(array: types, value: 0)
                signifers = appendIfNotExist(array: signifers,
                                             value: signifier?.rawValue,
                                             increment: 2)
            case .task(state: _, signifier: let signifier):
                types = appendIfNotExist(array: types, value: 1)
                signifers = appendIfNotExist(array: signifers,
                                             value: signifier?.rawValue,
                                             increment: 2)
            case .note(state: _, signifier: let signifier):
                signifers = appendIfNotExist(array: signifers,
                                             value: signifier?.rawValue,
                                             increment: 2)
            case .none:
                break
            }
        }
        let indices = types + signifers
        for index in indices {
            guard index >= 0, index < indicatorIcons.count else {
                continue
            }
            indicatorIcons[index].isHidden = false
        }
    }

    func updateSelectedUI(_ isSelected: Bool) {
        if isSelected {
            dateLabel.textColor = theme.color.onBackground
            dateLabel.font = theme.font.body2
        } else {
            dateLabel.textColor = theme.color.onBackground.withAlphaComponent(0.7)
            dateLabel.font = theme.font.body2
        }
    }
}
