//
//  MonthPickerCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 2/10/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class MonthPickerCoordinator: BaseCoordinator {
    let presentor: UIViewController
    let month: Month
    let sourceRect: CGRect?
    let sourceView: UIView?
    weak var delegate: MonthPickerDelegate?

    var controller: MonthPickerViewController?

    init(presentor: UIViewController,
         month: Month,
         sourceRect: CGRect?,
         sourceView: UIView?,
         delegate: MonthPickerDelegate?) {
        self.presentor = presentor
        self.month = month
        self.sourceRect = sourceRect
        self.sourceView = sourceView
        self.delegate = delegate
    }

    override func start() {
        let pickerController = MonthPickerViewController(nibName: String(describing: MonthPickerViewController.self), bundle: .main)
        pickerController.modalPresentationStyle = .popover
        let popover = pickerController.popoverPresentationController
        popover?.sourceView = sourceView
        popover?.sourceRect = sourceRect ?? .zero
        pickerController.preferredContentSize = CGSize(width: 320, height: 240)
        self.controller = pickerController

        let viewModel = MonthPickerViewModel(month: month)
        pickerController.viewModel = viewModel
        pickerController.delegate = delegate
        pickerController.coordinator = self
        presentor.present(pickerController, animated: true, completion: nil)
    }

    override func end() {
        controller?.dismiss(animated: true, completion: {
            super.end()
        })
    }
}
