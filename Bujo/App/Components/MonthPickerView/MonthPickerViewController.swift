//
//  MonthPickerViewController.swift
//  Bujo
//
//  Created by Khemarin on 2/10/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

protocol MonthPickerDelegate: class {
    func didTapDone(in picker: MonthPickerViewController, month: Month)
    func didTabCancel(in picker: MonthPickerViewController)
}

class MonthPickerViewController: UIViewController {
    @IBOutlet weak var pickerView: UIPickerView!

    weak var coordinator: MonthPickerCoordinator?
    var viewModel: MonthPickerViewModel?
    weak var delegate: MonthPickerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self
        reloadData(animated: false)
    }

    func reloadData(animated: Bool = true) {
        pickerView.reloadAllComponents()
        selectMonth(viewModel?.month, animted: animated)
    }

    private func selectMonth(_ month: Month?, animted: Bool) {
        if let index = viewModel?.indexOfMonth(month) {
            pickerView.selectRow(index.0, inComponent: 0, animated: animted)
            pickerView.selectRow(index.1, inComponent: 1, animated: animted)
        }
    }

    @IBAction func doneButtonTap() {
        guard let year = viewModel?.year(atIndex: pickerView.selectedRow(inComponent: 0)),
            let month = viewModel?.month(atIndex: pickerView.selectedRow(inComponent: 1)) else {
            return
        }
        delegate?.didTapDone(in: self, month: Month(year: year, month: month))
        coordinator?.end()
    }

    @IBAction func cancelButtonTap() {
        coordinator?.end()
    }

    @IBAction func thisMonthButtonTap() {
        selectMonth(Date().month, animted: true)
    }
}

extension MonthPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return MonthPickerViewModel.yearRange
        case 1:
            return MonthPickerViewModel.totalMonths
        default:
            return 0
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String? {

        switch component {
        case 0:
            return viewModel?.yearTitle(atIndex: row)
        case 1:
            return viewModel?.monthTitle(atIndex: row)
        default:
            return nil
        }
    }
}
