//
//  MonthPickerViewModel.swift
//  Bujo
//
//  Created by Khemarin on 2/10/19.
//  Copyright © 2019 Vemean. All rights reserved.
//
import Foundation

struct MonthPickerViewModel {
    var month: Month

    private(set) var monthNames: [String] = []
    private(set) var years: [Int] = []

    static let yearRange: Int = 25
    static let totalMonths: Int = 12

    init(month: Month) {
        self.month = month
        updateListData()
    }

    private mutating func updateListData() {
        monthNames = monthNameList()
        years = pickableYears()
    }

    private func monthNameList() -> [String] {
        return (1...12).compactMap { Month.monthName(of: $0) }
    }

    private func pickableYears() -> [Int] {
        let now = Date().month
        let minYear = min(month.year - 10, now.year)
        let maxYear = max(month.year + 15, now.year)
        return [Int](minYear...maxYear)
    }

    func year(atIndex index: Int) -> Int? {
        guard  index >= 0, index < years.count else {
            return nil
        }
        return years[index]
    }

    func yearTitle(atIndex index: Int) -> String? {
        if let yearInt = year(atIndex: index) {
            return "\(yearInt)"
        }
        return nil
    }

    func month(atIndex index: Int) -> Int? {
        guard index >= 0, index < monthNames.count else {
            return nil
        }
        return index + 1
    }

    func monthTitle(atIndex index: Int) -> String? {
        guard index >= 0, index < monthNames.count else {
            return nil
        }
        return monthNames[index]
    }

    /// Return index of month and year
    ///
    /// - Parameter month: `Month`
    /// - Returns: Index of month and  year
    func indexOfMonth(_ monthToCalculate: Month?) -> (Int, Int)? {
        guard let month = monthToCalculate else {
            return nil
        }
        let monthIndex = month.month - 1
        guard let yearIndex = years.firstIndex(of: month.year) else {
            return nil
        }
        return (yearIndex, monthIndex)
    }
}
