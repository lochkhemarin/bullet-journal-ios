//
//  UIViewControllerExtensions.swift
//  InteractiveTransition
//
//  Created by Khemarin on 4/30/19.
//  Copyright © 2019 vemean. All rights reserved.
//

import UIKit

extension InteractivePresentor where Self: UIViewController {
    var frame: CGRect {
        return view.frame.inset(by: view.safeAreaInsets)
    }

    func addInteractiveController(_ controller: InteractiveController, percentageSteps: [CGFloat]) {
        addChild(controller)
        controller.steps = .percentage(percentageSteps.sorted())
        controller.presentor = self
        controller.didMove(toParent: self)
    }

    func addInteractiveController(_ controller: InteractiveController, distanceSteps: [CGFloat]) {
        addChild(controller)
        controller.steps = .distance(distanceSteps.sorted())
        controller.presentor = self
        controller.didMove(toParent: self)
    }
}
