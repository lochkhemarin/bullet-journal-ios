//
//  InteractiveController.swift
//  InteractiveTransition
//
//  Created by Khemarin on 4/30/19.
//  Copyright © 2019 vemean. All rights reserved.
//

import UIKit

protocol InteractivePresentor: class {
    var frame: CGRect { get }
}

protocol InteractiveControllerDelegate: class {
    func interactiveController(_ controller: InteractiveController, didFinishAnimatingToStep step: Int)
    func interactiveController(_ controller: InteractiveController, willStartAnimatingToStep step: Int)
}

extension InteractiveControllerDelegate {
    func interactiveController(_ controller: InteractiveController, didFinishAnimatingToStep step: Int) {
        // Optional implementation
    }

    func interactiveController(_ controller: InteractiveController, willStartAnimatingToStep step: Int) {
        // Optional implementation
    }
}

class InteractiveController: UIViewController {
    var handlerView = UIView()

    var handlerOptions = HandlerOptions()

    weak var presentor: InteractivePresentor?
    weak var delegate: InteractiveControllerDelegate?

    private var levels: [LevelConstraint] = []
    private var levelViews: [UIView] = []

    var steps: Step = .percentage([])
    private var percentSteps: [CGFloat] = []

    private var currentStep: Int?

    private var panGesture: UIPanGestureRecognizer?
    private var previousStep: Int?
    private var startedPointInView: CGPoint?

    override func viewDidLoad() {
        super.viewDidLoad()

        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panHandler(_:)))
        view.addGestureRecognizer(panGesture)
        self.panGesture = panGesture

        view.addSubview(handlerView)
        view.layer.cornerRadius = 15.0
        currentStep = 0
        recalculateSteps()
        setupHandlerView()
    }

    func setLevels(_ levels: [LevelConstraint]) {
        guard let parentView = (presentor as? UIViewController)?.view else {
            return
        }
        self.levels = levels
        self.levelViews = []
        for level in levels {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            parentView.addSubview(view)
            view.backgroundColor = UIColor.black

            view.leadingAnchor.constraint(equalTo: parentView.leadingAnchor).isActive = true
            view.trailingAnchor.constraint(equalTo: parentView.trailingAnchor).isActive = true
            view.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            view.topAnchor.constraint(equalTo: level.anchor, constant: level.constant).isActive = true
            levelViews.append(view)
        }
    }

    fileprivate func recalculateSteps() {
        guard let parentFrame = presentor?.frame, parentFrame.height != 0 else {
            return
        }
        switch steps {
        case .distance(let distances):
            percentSteps = []
            let height = parentFrame.height
            for distance in distances {
                percentSteps.append(distance / height)
            }
        case .percentage(let steps):
            self.percentSteps = steps
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        recalculateSteps()
    }

    func setupHandlerView() {
        handlerView.layer.backgroundColor = handlerOptions.backgroundColor.cgColor
        handlerView.layer.cornerRadius = handlerOptions.corner
        setHandlerFrame()
    }

    fileprivate func setHandlerFrame() {
        let size = handlerOptions.size
        handlerView.frame = CGRect(x: (view.frame.size.width - size.width) / 2,
                                   y: handlerOptions.yPosition,
                                   width: size.width,
                                   height: size.height)
    }

    /// Settle the ui with current selected step
    func layoutWithCurrentStep() {
        recalculateSteps()
        if let step = currentStep, let frame = frame(forStep: step) {
            view?.frame = frame
        }
    }

    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        parent?.view.addSubview(view)
        if !percentSteps.isEmpty, let frame = frame(forStep: 0) {
            view?.frame = frame
            currentStep = 0
        }
    }

    private func frame(forStep step: Int) -> CGRect? {
        guard step < percentSteps.count, let parentFrame = presentor?.frame  else {
            return nil
        }
        let percentage = percentSteps[step]
        let origin = CGPoint(x: 0, y: parentFrame.height * (1 - percentage))
        return CGRect(origin: origin, size: parentFrame.size)
    }
}

// MARK: - Pan handler
extension InteractiveController {
    @objc func panHandler(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began, .possible:
            previousStep = currentStep
            startedPointInView = gesture.location(in: view)
        case .ended:
            let velocity = gesture.velocity(in: view).y
            var index = currentStep ?? 0
            let velocityValue = abs(velocity)
            if velocityValue >= 2 {
                index -= Int(velocity / velocityValue)
                animateToStep(index) { [weak self] finished, step in
                    if finished {
                        self?.currentStep = step
                    }
                }
            } else {
                let point = gesture.location(in: parent?.view)
                animateToClosestStep(from: point) { [weak self] finished, step in
                    if finished {
                        self?.currentStep = step
                    }
                }
            }
            fallthrough
        case .cancelled, .failed:
            previousStep = nil
            startedPointInView = nil
        case .changed:
            let point = gesture.location(in: parent?.view)
            adjustFrameWhenDrag(point: point)
        }
    }

    /// Adjust view's frame base on provided touch point related to parent's view
    ///
    /// - Parameter point: Point
    private func adjustFrameWhenDrag(point: CGPoint) {
        guard let parentFrame = presentor?.frame, parentFrame.height != 0 else {
            return
        }
        let percentage = 1 - ((point.y - (startedPointInView?.y ?? 0)) / parentFrame.height)
        let origin = CGPoint(x: 0, y: parentFrame.height * (1 - percentage))
        view.frame = CGRect(origin: origin, size: parentFrame.size)
    }

    private func animateToClosestStep(from point: CGPoint, completion: ((Bool, Int) -> Void)? = nil) {
        guard let parentFrame = presentor?.frame, parentFrame.height != 0,
            !percentSteps.isEmpty, let maxStep = percentSteps.last else {
            return
        }
        let percentage = 1 - ((point.y - (startedPointInView?.y ?? 0)) / parentFrame.height)
        var diff: CGFloat = 2
        var closestStep = 0
        if percentage > maxStep {
            closestStep = percentSteps.count - 1
        } else if percentage > 0 {
            for (index, step) in percentSteps.enumerated() {
                let currentDiff = abs(step - percentage)
                if currentDiff > diff {
                    break
                } else {
                    closestStep = index
                    diff = currentDiff
                }
            }
        }
        animateToStep(closestStep) { finished, step in
            completion?(finished, step)
        }
    }

    func animateToStep(_ step: Int, completion: ((Bool, Int) -> Void)? = nil) {
        guard let parentFrame = presentor?.frame, step >= 0, step < percentSteps.count else {
            return
        }
        var index = step
        if step >= percentSteps.count {
            index = percentSteps.count - 1
        } else if step < 0 {
            index = 0
        }
        let percentage = percentSteps[index]
        let origin = CGPoint(x: 0, y: parentFrame.height * (1 - percentage))
        delegate?.interactiveController(self, willStartAnimatingToStep: index)

        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: .curveEaseOut,
            animations: { [weak self] in
                self?.view.frame = CGRect(origin: origin, size: parentFrame.size)
            },
            completion: { [weak self] finished in
                completion?(finished, index)
                guard let self = self else {
                    return
                }
                self.currentStep = index
                self.delegate?.interactiveController(self, didFinishAnimatingToStep: index)
        })
    }
}

extension InteractiveController {
    enum Step {
        case percentage([CGFloat])
        case distance([CGFloat])
    }

    struct LevelConstraint {
        let anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>
        let constant: CGFloat

        init(anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant: CGFloat) {
            self.anchor = anchor
            self.constant = constant
        }
    }
}

struct HandlerOptions {
    var size: CGSize
    var corner: CGFloat
    var backgroundColor: UIColor
    var yPosition: CGFloat

    init() {
        size = CGSize(width: 30, height: 5)
        corner = 2.5
        backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        yPosition = 5
    }
}
