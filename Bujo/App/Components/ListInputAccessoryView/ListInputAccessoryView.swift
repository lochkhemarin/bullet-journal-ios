//
//  ListInputAccessoryView.swift
//  Bujo
//
//  Created by Khemarin on 10/25/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

protocol ListInputAccessoryViewDelegate: class {
    func didTapButtonType(_ type: TodoListViewModel.ListType, in view: ListInputAccessoryView)
    func didTapSignifier(_ signifier: TodoListViewModel.Signifier?, in view: ListInputAccessoryView)
}

class ListInputAccessoryView: UIView {
    @IBOutlet weak var crownButton: UIButton!
    @IBOutlet weak var starButton: UIButton!

    @IBOutlet weak var taskButton: UIButton!
    @IBOutlet weak var eventButton: UIButton!
    @IBOutlet weak var noteButton: UIButton!

    static let signifierHighlightedColor = UIColor(white: 0.8, alpha: 0.2)

    weak var delegate: ListInputAccessoryViewDelegate?
    /// Current task state of the accessory view. Ideally, it is the toggled of the paragraph context.
    private var task: TodoListViewModel.ListType = .task(state: .normal, signifier: nil) {
        didSet {
            updateTaskIcon()
        }
    }

    /// Current event state of the accessory view. Ideally, it is the toggled of the paragraph context.
    private var event: TodoListViewModel.ListType = .event(state: .normal, signifier: nil) {
        didSet {
            updateEventIcon()
        }
    }

    /// Current event state of the accessory view. Ideally, it is the toggled of the paragraph context.
    private var note: TodoListViewModel.ListType = .note(state: .normal, signifier: nil) {
        didSet {
            updateNoteIcon()
        }
    }

    private var signifier: TodoListViewModel.Signifier? {
        didSet {
            updateSignifierIcon()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        updateTaskIcon()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    @IBAction func taskTap() {
        delegate?.didTapButtonType(task, in: self)
    }

    @IBAction func eventTap() {
        delegate?.didTapButtonType(event, in: self)
    }

    @IBAction func noteTap() {
        delegate?.didTapButtonType(note, in: self)
    }

    @IBAction func crownTap() {
        if signifier != .inspiration {
            delegate?.didTapSignifier(.inspiration, in: self)
        } else {
            delegate?.didTapSignifier(nil, in: self)
        }
    }

    @IBAction func starTap() {
        if signifier != .important {
            delegate?.didTapSignifier(.important, in: self)
        } else {
            delegate?.didTapSignifier(nil, in: self)
        }
    }

    private func updateTaskIcon() {
        let icon = TodoListViewModel.listImageForType(task)
        taskButton?.setImage(icon, for: .normal)
    }

    private func updateEventIcon() {
        let icon = TodoListViewModel.listImageForType(event)
        eventButton?.setImage(icon, for: .normal)
    }

    private func updateNoteIcon() {
        let icon = TodoListViewModel.listImageForType(note)
        noteButton?.setImage(icon, for: .normal)
    }

    private func updateSignifierIcon() {
        crownButton.backgroundColor = .clear
        starButton.backgroundColor = .clear
        guard let currentSignifier = signifier else {
            return
        }

        switch currentSignifier {
        case .important:
            starButton.backgroundColor = ListInputAccessoryView.signifierHighlightedColor
        case .inspiration:
            crownButton.backgroundColor = ListInputAccessoryView.signifierHighlightedColor
        }
    }

    /// Use this method to change context of the input.
    ///
    /// Set type as an array. Order won't be effect the display.
    /// Signifier of type won't take into account.
    ///
    /// - Parameters:
    ///   - types: [TodoListViewModel.ListType]
    ///   - signifiers: TodoListViewModel.Signifier
    func setContextType(types: [TodoListViewModel.ListType], signifier: TodoListViewModel.Signifier?) {
        for type in types {
            switch type {
            case .task(state: _, signifier: _):
                task = type
            case .event(state: _, signifier: _):
                event = type
            case .note(state: _, signifier: _):
                note = type
            case .none:
                break
            }
        }
        self.signifier = signifier
    }
}
