//
//  ListInputAccessoryViewModel.swift
//  Bujo
//
//  Created by Khemarin on 10/31/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

class ListInputAccessoryViewModel {
    static func toggleStateForType(_ type: TodoListViewModel.ListType) -> TodoListViewModel.ListType {
        switch type {
        case .task(state: let state, signifier: let signifier):
            return .task(state: toggleState(state), signifier: signifier)
        case .event(state: let state, signifier: let signifier):
            return .event(state: toggleState(state), signifier: signifier)
        case .note(state: _, signifier: _), .none:
            return type
        }
    }

    private static func toggleState(_ state: TodoListViewModel.ListState) -> TodoListViewModel.ListState {
        switch state {
        case .normal:
            return .completed
        case .completed, .canceled:
            return .normal
        }
    }
}
