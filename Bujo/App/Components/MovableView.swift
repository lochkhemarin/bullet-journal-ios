//
//  MovableView.swift
//  Bujo
//
//  Created by Khemarin on 10/4/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

protocol MovableDelegate: class {
    func didFinishDragging(component: MovableView, translate: CGPoint, state: UIPanGestureRecognizer.State)
    func didFinishResizing(component: MovableView)
    func startResizingMode(component: MovableView)
}

class MovableView: UIView {
    static let minWidth: CGFloat = 200
    static let minHeight: CGFloat = 230
    static let defaultBackground = UIColor.clear
    static let dragModeBackground = UIColor(white: 0.9, alpha: 0.4)
    static let resizeModeBackground = UIColor(white: 0.8, alpha: 0.5)

    private var frameWhenStartResizing: CGRect = .zero

    weak var delegate: MovableDelegate?
    private var panGesture: UIPanGestureRecognizer?
    var longPressGesture: UILongPressGestureRecognizer?
    private var startMoveLocation: CGPoint?

    private var resizeIcon: UIImageView?
    var isResizingMode: Bool = false {
        didSet {
            updateBackgroundColor()
            resizeIcon?.isHidden = !isResizingMode
            resizeIcon?.isUserInteractionEnabled = isResizingMode
            setPanGestureStatus()
            longPressGesture?.isEnabled = !isResizingMode
        }
    }

    var isResizeEnable: Bool = true

    var isDragEnabled: Bool = true {
        didSet {
            setPanGestureStatus()
            dragEnabledDidChange()
            updateBackgroundColor()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = MovableView.defaultBackground
        clipsToBounds = false
        layer.cornerRadius = 3

        setupResizeIcon()
        configurePanGesture()
        configureLongPressGesture()
    }

    private func setupResizeIcon() {
        let resizeImageView = UIImageView(image: Asset.drag.image)
        resizeIcon = resizeImageView
        resizeImageView.isHidden = true
        addSubview(resizeImageView)
        let resizeGesture = UIPanGestureRecognizer(target: self, action: #selector(resizeHandler(gesture:)))
        resizeIcon?.addGestureRecognizer(resizeGesture)
        resizeIcon?.isUserInteractionEnabled = true
    }

    private func resizeIconFrame() -> CGRect {
        guard let resizeImageView = resizeIcon else {
            return .zero
        }
        let size = resizeImageView.frame.size
        return CGRect(x: frame.width - 5 - size.width / 2,
               y: frame.height / 2,
               width: size.width,
               height: size.height)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Configure gesture and add to super view.
    ///
    /// - Parameters:
    ///   - superView: Super view of the component
    ///   - delegate: Delegate handler
    func configure(superView: UIView, delegate: MovableDelegate) {
        superView.addSubview(self)
        self.delegate = delegate
    }

    func dragEnabledDidChange() {}
    func didFinishResizing() {}
    func resizing() {}

    func beginDragging() {
        guard !isResizingMode else {
            return
        }
        backgroundColor = MovableView.dragModeBackground
    }

    func didFinishDragging() {
        guard !isResizingMode else {
            return
        }
        backgroundColor = MovableView.defaultBackground
    }

    // MARK: - UI

    private func updateBackgroundColor() {
        guard !isResizingMode else {
            backgroundColor = MovableView.resizeModeBackground
            return
        }
        backgroundColor = MovableView.defaultBackground
    }

    // MARK: - Gestures

    private func setPanGestureStatus() {
        if isResizingMode {
            panGesture?.isEnabled = true
        } else {
            panGesture?.isEnabled = isDragEnabled
        }
    }

    private func configurePanGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panHandler(gesture:)))
        addGestureRecognizer(panGesture)
        self.panGesture = panGesture
    }

    private func configureLongPressGesture() {
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressHandler(gesture:)))
        addGestureRecognizer(longPressGesture)
        self.longPressGesture = longPressGesture
    }

    @objc private func longPressHandler(gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .began, isResizeEnable else {
            return
        }
        if !isResizingMode {
            isResizingMode = true
            resizeIcon?.frame = resizeIconFrame()
            delegate?.startResizingMode(component: self)
        }
    }

    @objc private func panHandler(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            beginDragging()
        case .changed:
            let translated = translateForGesture(gesture, in: superview)
            self.drag(translate: translated, zIndex: 0)
        case .ended:
            let translated = translateForGesture(gesture, in: superview)
            delegate?.didFinishDragging(component: self, translate: translated, state: gesture.state)
            didFinishDragging()
            finishTranslation()

            startMoveLocation = nil
        default:
            break
        }
    }

    @objc private func resizeHandler(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            frameWhenStartResizing = frame
        case .changed:
            let translated = translateForGesture(gesture, in: superview)
            let newWidth = frameWhenStartResizing.width + translated.x
            let newSize = CGSize(width: newWidth, height: frameWhenStartResizing.height)
            let newFrame = CGRect(origin: frameWhenStartResizing.origin, size: newSize)
            frame = newFrame
            resizing()
            setNeedsDisplay()

            resizeIcon?.layer.transform = CATransform3DTranslate(CATransform3DIdentity, translated.x, 0, 0)
        case .ended:
            frameWhenStartResizing = .zero

            // Allow inheritant to change the size
            didFinishResizing()

            resizeIcon?.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
            resizeIcon?.frame = resizeIconFrame()

            delegate?.didFinishResizing(component: self)
        default:
            break
        }
    }

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    /// Calculate translation for gesture.
    ///
    /// *Notes:* If gesture is longpress, translation is calculated base on `startMoveLocation`.
    /// This property must be set in advanced.
    ///
    /// - Parameters:
    ///   - gesture: gesture
    ///   - view: base
    /// - Returns: transated point
    private func translateForGesture(_ gesture: UIGestureRecognizer, in view: UIView?) -> CGPoint {
        if let panGesture = gesture as? UIPanGestureRecognizer {
            return panGesture.translation(in: view)
        } else if let longPress = gesture as? UILongPressGestureRecognizer {
            let point = longPress.location(in: superview)
            if let start = startMoveLocation {
                return CGPoint(x: point.x - start.x, y: point.y - start.y)
            } else {
                return point
            }
        }
        return CGPoint.zero
    }

    private func finishTranslation() {
        let origin = frame.origin
        layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
        self.frame = CGRect(origin: origin, size: frame.size)
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // Increase chance of hitting resize icon
        if let resizeImageView = resizeIcon, resizeImageView.isUserInteractionEnabled {
            let extendedResizeIconFrame = resizeImageView.frame.insetBy(dx: -10, dy: -10)
            if extendedResizeIconFrame.contains(point) {
                return resizeImageView
            }
        }

        return super.hitTest(point, with: event)
    }
}

extension MovableView {
    /// Update component's position
    ///
    /// - Parameter point: target position
    fileprivate func drag(translate: CGPoint, zIndex: CGFloat) {
        layer.transform = CATransform3DTranslate(CATransform3DIdentity, translate.x, translate.y, zIndex)
    }

    /// Rotate component
    ///
    /// - Parameter angle: target angle in radian
    fileprivate func rotate(to angle: CGFloat) {
        layer.transform = CATransform3DRotate(CATransform3DIdentity, angle, frame.midX, frame.midY, 0)
    }

    /// Scale component
    ///
    /// - Parameters:
    ///   - x: target scale x axis
    ///   - y: target scale y axis
    fileprivate func scale(x: CGFloat, y: CGFloat) {
        layer.transform = CATransform3DScale(CATransform3DIdentity, x, y, 1)
    }
}
