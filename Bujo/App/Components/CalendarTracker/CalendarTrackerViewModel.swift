//
//  CalendarTrackerViewModel.swift
//  Bujo
//
//  Created by Khemarin on 12/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class CalendarTrackerViewModel {
    var identifier: String?
    static let summaryTitle = "Total"
    var title: String? {
        didSet {
            titleDidChange?()
        }
    }

    var selectedDays: [Int] = [] {
        didSet {
            selectedDidSet?()
        }
    }
    var calendar: CalendarGrid {
        didSet {
            updateCalendar()
            calendarDidSet?()
        }
    }
    fileprivate var maxDate: Int = 0

    typealias VoidCallback = () -> Void
    var calendarDidSet: VoidCallback?
    var selectedDidSet: VoidCallback?
    var titleDidChange: VoidCallback?

    var month: Month {
        didSet {
            calendar = CalendarGrid(baseMonth: month)
        }
    }

    init(month: Month) {
        self.month = month
        calendar = CalendarGrid(baseMonth: month)
        updateCalendar()
    }

    init(model: CalendarTrackerModel) {
        let month = model.date.month
        title = model.title
        self.month = month
        calendar = CalendarGrid(baseMonth: month)
        updateCalendar()
        self.selectedDays = model.selectedDay.components(separatedBy: ",").compactMap { Int($0) }
        self.identifier = model.id
    }

    fileprivate func updateCalendar() {
        calendar.useShortWeekday = true
        maxDate = calendar.grid?.last?.max() ?? 0
    }

    func toggleSelectAt(day: Int) {
        guard day > 0, day <= maxDate else {
            return
        }
        if let index = selectedDays.firstIndex(of: day) {
            selectedDays.remove(at: index)
        } else {
            selectedDays.append(day)
        }
    }

    func isToday(_ day: Int) -> Bool {
        let date = Date()
        let thisMonth = date.month.month
        let today = Calendar.current.component(.day, from: date)
        return month.month == thisMonth && day == today
    }
}

// MARK: - Local Storage
extension CalendarTrackerViewModel {
    var asModel: CalendarTrackerModel? {
        guard let date = month.firstDayDate else {
            return nil
        }
        let model = CalendarTrackerModel()
        if let identifier = self.identifier {
            model.id = identifier
        }
        model.title = title ?? ""
        model.date = date
        model.selectedDay = selectedDays.compactMap { "\($0)" }.joined(separator: ",")
        return model
    }
}
