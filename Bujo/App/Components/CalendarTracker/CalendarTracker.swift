//
//  CalendarTracker.swift
//  Bujo
//
//  Created by Khemarin on 12/19/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

protocol CalendarTrackerDelegate: class {
    func calendarTrackerDayDidTap(_ tracker: CalendarTracker, at day: Int)
    func calendarTrackerDeleteDidTap(_ tracker: CalendarTracker)
    func calendarTrackerTitleChanged(_ tracker: CalendarTracker)
    func calendarTrackerTitleDidBeginEditing(_ tracker: CalendarTracker)
}

class CalendarTracker: UIView {
    @IBOutlet weak var titleInputText: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var summaryButton: UIButton!
    @IBOutlet weak var summaryTitle: UILabel!
    fileprivate let labelHeightAndMargin: CGFloat = 50
    fileprivate let calendarMargin: CGFloat = 0
    fileprivate let calendarBottomMargin: CGFloat = 25
    fileprivate var cellSize: CGSize = CGSize.zero
    fileprivate var horizontalSpacing: CGFloat = 0

    var theme: BujoTheme = BujoTheme.shared

    var viewModel: CalendarTrackerViewModel {
        didSet {
            updateTitle()
            updateSummaryTitle()
            render()
        }
    }

    weak var delegate: CalendarTrackerDelegate?

    fileprivate let cellIdentifier = "CalendarTrackerDayCell"

    var month: Month {
        didSet {
            viewModel.month = month
        }
    }

    override var frame: CGRect {
        didSet {
            updateCellSize()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        self.month = Month(year: 1970, month: 1)
        viewModel = CalendarTrackerViewModel(month: month)
        super.init(coder: aDecoder)
        commonInit()
    }

    init(frame: CGRect, month: Month) {
        self.month = month
        viewModel = CalendarTrackerViewModel(month: month)
        super.init(frame: frame)
        commonInit()
    }

    private func commonInit() {
        fromNib()
        viewModel = CalendarTrackerViewModel(month: month)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.clipsToBounds = false
        collectionView.register(UINib(nibName: cellIdentifier, bundle: .main),
                                forCellWithReuseIdentifier: cellIdentifier)
        setupViewModelCallback()
        summaryTitle.text = CalendarTrackerViewModel.summaryTitle
        titleInputText.delegate = self
        updateCellSize()
        setupTheme()
        render()
    }

    private func setupTheme() {
        titleInputText.font = theme.font.body2
        titleInputText.textColor = theme.color.onBackground

        summaryTitle.font = theme.font.caption
        summaryTitle.textColor = theme.color.onBackground
        summaryButton.titleLabel?.font = theme.font.body2
        summaryButton.setTitleColor(theme.color.secondary, for: .normal)
    }

    private func updateCellSize() {
        let columns = CGFloat(viewModel.calendar.grid?.first?.count ?? 0)
        let rows = CGFloat(viewModel.calendar.grid?.count ?? 0) + 1
        guard columns > 0, rows > 0 else {
            return
        }

        let gridWidth = bounds.width - (calendarMargin * 2)
        let width = gridWidth / columns
        let height = (bounds.height - labelHeightAndMargin - calendarBottomMargin) / rows
        cellSize = CGSize(width: width, height: height)
        horizontalSpacing = (gridWidth - (width * columns)) / columns
    }

    func render() {
        collectionView.reloadData()
        updateSummaryTitle()
    }

    fileprivate func updateSummaryTitle() {
        summaryButton.setTitle("\(viewModel.selectedDays.count)", for: .normal)
    }

    fileprivate func updateTitle() {
        titleInputText.text = viewModel.title
    }

    private func setupViewModelCallback() {
        viewModel.calendarDidSet = { [weak self] in
            self?.render()
        }
        viewModel.selectedDidSet = { [weak self] in
            self?.updateSummaryTitle()
        }

        viewModel.titleDidChange = { [weak self] in
            self?.updateTitle()
        }
    }
}

extension CalendarTracker: UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let grid = viewModel.calendar.grid, section < grid.count + 1 else {
            return 0
        }
        if section == 0 {
            return  viewModel.calendar.weekdaySymbols.count
        } else {
            return grid[section - 1].count
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (viewModel.calendar.grid?.count ?? 0) + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        guard let grid = viewModel.calendar.grid, indexPath.section < grid.count + 1 else {
            return cell
        }

        guard let trackerCell = cell as? CalendarTrackerDayCell else {
            return cell
        }

        trackerCell.valueLabel.font = theme.font.body1

        let section = indexPath.section
        trackerCell.isDone = false
        if section == 0 {
            if indexPath.item < viewModel.calendar.weekdaySymbols.count {
                trackerCell.valueLabel.text = viewModel.calendar.weekdaySymbols[indexPath.item]
            }
            trackerCell.valueLabel.textColor = theme.color.onBackground.withAlphaComponent(0.7)
        } else {
            //section already != 0 and < grid.count + 1
            if indexPath.item < grid[section - 1].count {
                let day = grid[section - 1][indexPath.item]
                trackerCell.valueLabel.text = day == 0 ? "" : "\(day)"
                trackerCell.isDone = viewModel.selectedDays.contains(day) == true

                trackerCell.valueLabel.textColor = viewModel.isToday(day) ? theme.color.onBackground : theme.color.onBackground.withAlphaComponent(0.7)
            }
        }
        return trackerCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return horizontalSpacing
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let grid = viewModel.calendar.grid, indexPath.section < grid.count + 1 else {
            return
        }

        let section = indexPath.section
        guard section != 0 else {
            return
        }
        //section already != 0 and < grid.count + 1
        if indexPath.item < grid[section - 1].count {
            let day = grid[section - 1][indexPath.item]
            if day != 0 {
                delegate?.calendarTrackerDayDidTap(self, at: day)
            }
        }
    }

    @IBAction func deleteButtonAction() {
        delegate?.calendarTrackerDeleteDidTap(self)
    }
}

extension CalendarTracker: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == titleInputText {
            viewModel.title = textField.text
            delegate?.calendarTrackerTitleChanged(self)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == titleInputText {
            delegate?.calendarTrackerTitleDidBeginEditing(self)
        }
    }
}
