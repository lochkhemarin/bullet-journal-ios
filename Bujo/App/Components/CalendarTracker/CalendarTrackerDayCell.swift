//
//  CalendarTrackerDayCell.swift
//  Bujo
//
//  Created by Khemarin on 12/20/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class CalendarTrackerDayCell: UICollectionViewCell {
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var doneImage: UIImageView!

    var isDone: Bool = false {
        didSet {
            doneImage.isHidden = !isDone
        }
    }

    let theme = BujoTheme.shared

    override func awakeFromNib() {
        super.awakeFromNib()
        valueLabel.isUserInteractionEnabled = false
        valueLabel.textAlignment = .center

        doneImage.isUserInteractionEnabled = false
        clipsToBounds = false
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        valueLabel.text = nil
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        doneImage.frame = bounds
        valueLabel.frame = doneImage.frame
    }
}
