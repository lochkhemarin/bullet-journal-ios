//
//  TopHeaderViewModel.swift
//  Bujo
//
//  Created by Khemarin on 1/14/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class TopHeaderViewModel {
    var screen: Screen
    var month: Month?

    init(screen: Screen) {
        self.screen = screen
    }

    func monthTitle() -> String? {
        guard let month = month else {
            return nil
        }
        let monthName = Month.monthName(of: month.month)
        return "\(monthName) \(month.year)"
    }

    func newButtonTitle() -> String {
        switch screen {
        case .home:
            return "New List"
        case .tracker:
            return "New Tracker"
        case .doodle, .tabbar, .help:
            return ""
        }
    }

    func newButtonIcon() -> UIImage? {
        switch screen {
        case .home:
            return Asset.list.image
        case .tracker:
            return Asset.calendarTracker.image
        case .doodle, .tabbar, .help:
            return nil
        }
    }
}
