//
//  TopHeaderView.swift
//  Bujo
//
//  Created by Khemarin on 1/4/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

protocol TopHeaderViewDelegate: class {
    func topHeaderDidTapNew(in view: TopHeaderView)
    func topHeaderDidTapDate(in view: TopHeaderView)
}

class TopHeaderView: UIView {

    var screen: Screen = .tabbar {
        didSet {
            viewModel?.screen = screen
            render()
        }
    }
    var month: Month? {
        didSet {
            viewModel?.month = month
            render()
        }
    }

    private var viewModel: TopHeaderViewModel?

    weak var delegate: TopHeaderViewDelegate?

    @IBOutlet weak var newElementButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!

    var theme = BujoTheme.shared

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    fileprivate func commonInit() {
        fromNib()
        viewModel = TopHeaderViewModel(screen: screen)
        render()
    }

    func render() {
        newElementButton.setTitleColor(theme.color.primary, for: .normal)
        newElementButton.titleLabel?.font = theme.font.button
        newElementButton.setImage(viewModel?.newButtonIcon(), for: .normal)
        newElementButton.setTitle(viewModel?.newButtonTitle(), for: .normal)

        dateButton.setTitleColor(theme.color.primary, for: .normal)
        dateButton.titleLabel?.font = theme.font.button
        dateButton.setTitle(viewModel?.monthTitle(), for: .normal)
    }

    @IBAction func tapNewAction() {
        delegate?.topHeaderDidTapNew(in: self)
    }

    @IBAction func tabDateAction() {
        delegate?.topHeaderDidTapDate(in: self)
    }
}
