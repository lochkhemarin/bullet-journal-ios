//
//  Curlyline.swift
//  Bujo
//
//  Created by Khemarin on 10/12/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

/// In progress, do not use.
class Curlyline: UIBezierPath {
    enum Axis {
        case x
        case y
    }

    let axis: Axis

    init(axis: Axis, length: CGFloat, lineWidth: CGFloat) {
        self.axis = axis

        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        axis = .x
        super.init(coder: aDecoder)
    }
}
