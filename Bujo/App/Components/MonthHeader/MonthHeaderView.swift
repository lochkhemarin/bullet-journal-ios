//
//  MonthHeaderView.swift
//  Bujo
//
//  Created by Khemarin on 1/27/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit
import Stevia

class MonthHeaderView: UIView {
    var month: Int {
        didSet {
            render()
        }
    }

    let imageView: UIImageView

    /// Init view with frame and month
    ///
    /// - Parameters:
    ///   - frame: View's rectangle
    ///   - month: Month (1-12)
    init(frame: CGRect, month: Int) {
        guard month > 0, month < 13 else {
            fatalError("Month should be from 1-12")
        }
        self.month = month
        self.imageView = UIImageView()
        super.init(frame: frame)

        sv(imageView)
        imageView.contentMode = .scaleAspectFit
        layout(
            0,
            |-0-imageView-0-|,
            0
        )

        render()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UI
    private func render() {
        imageView.image = nil
        let imageName = "month_\(month)"
        DispatchQueue.global(qos: .background).async {
            let image = UIImage(named: imageName)
            DispatchQueue.main.async { [weak self] in
                self?.imageView.image = image
            }
        }
    }

}
