//
//  TodoListView.swift
//  Bujo
//
//  Created by Khemarin on 10/16/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

protocol TodoListViewDelegate: class {
    func textDidBeginEditing(listView: TodoListView)
}

class TodoListView: MovableView {
    var textInput: ListInputView?
    var headerView: SimpleListHeader?
    weak var textDelegate: TodoListViewDelegate?
    private var tapGesture: UITapGestureRecognizer?
    private var paragrahStyle: NSMutableParagraphStyle = {
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        style.paragraphSpacing = 10
        style.lineSpacing = 6
        return style
    }()

    var theme: BujoTheme = BujoTheme.shared

    private lazy var textAttributes: [NSAttributedString.Key: Any] = {
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.paragraphStyle: paragrahStyle,
            NSAttributedString.Key.strikethroughStyle: 0,
            NSAttributedString.Key.font: theme.font.body2,
            NSAttributedString.Key.foregroundColor: UIColor.darkText
        ]
        return attributes
    }()

    private lazy var completedTextAttributes: [NSAttributedString.Key: Any] = {
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.paragraphStyle: paragrahStyle,
            NSAttributedString.Key.strikethroughStyle: 1,
            NSAttributedString.Key.font: Font.scriptFont(ofSize: TodoListView.fontSize),
            NSAttributedString.Key.foregroundColor: UIColor.darkGray
        ]
        return attributes
    }()

    private var listTypeIcons: [UIImageView] = []
    private var signifierIcons: [UIImageView] = []
    private let listTypeIconSize = CGSize(width: 20, height: 20)
    private var previousRect: CGRect = .zero

    private var forceUpdateTextInputRectsOnDidChange = false

    static let fontSize: CGFloat = 17.0
    static let iconSpace: CGFloat = 50.0
    static let headerHeight: CGFloat = 45.0

    var viewModel = TodoListViewModel() {
        didSet {
            if viewModel.paragraphTypes.isEmpty {
                viewModel.paragraphTypes = viewModel.paragraphTypes(for: viewModel.text ?? "", types: [])
            }
            textInput?.attributedText = NSAttributedString(string: viewModel.text ?? "", attributes: textAttributes)
            for index in 0..<viewModel.paragraphTypes.count {
                guard let state = viewModel.paragraphTypes[index].type.state, state == .completed else {
                    continue
                }
                updateTextAttribute(atParagraph: index)
            }
            headerView?.text = viewModel.titleHeader
        }
    }

    /// Initialize component with frame.
    ///
    /// There will be free space on the right of text editor for checkmark and other icons.
    ///
    /// - Parameter frame: Frame
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupEditor(frame: frame)
        //setupHeader(frame: frame)
    }

    private func setupHeader(frame: CGRect) {
        guard let header: SimpleListHeader = SimpleListHeader.fromNib() else {
            return
        }
        headerView = header
        header.titleInput.placeholder = "Add title here"
        header.titleInput.delegate = self

        header.frame = calculateHeaderFrame()
        addSubview(header)
        header.text = viewModel.titleHeader
    }

    private func setupEditor(frame: CGRect) {
        let editorFrame = calculateEditorFrame()
        let editor = ListInputView(frame: editorFrame)
        addSubview(editor)
        editor.isEditable = true
        editor.backgroundColor = .clear
        editor.delegate = self
        editor.autocorrectionType = .no
        editor.listDelegate = self
        editor.typingAttributes = textAttributes

        textInput = editor
        textInput?.isUserInteractionEnabled = false
        let accessoryView: ListInputAccessoryView? = ListInputAccessoryView.fromNib()
        accessoryView?.delegate = self
        textInput?.inputAccessoryView = accessoryView

        let tapGesturerRecognizer = UITapGestureRecognizer(target: self,
                                                           action: #selector(tapGestureHandler(tapGesture:)))
        tapGesturerRecognizer.numberOfTapsRequired = 1
        tapGesturerRecognizer.numberOfTouchesRequired = 1
        addGestureRecognizer(tapGesturerRecognizer)

        // Change priorty
        if let longPressGesture = self.longPressGesture {
            tapGesturerRecognizer.require(toFail: longPressGesture)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func dragEnabledDidChange() {

    }

    override func didFinishResizing() {
        updateFrameWithCurrentText()
    }

    override func resizing() {
        updateFrameWithCurrentText()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateFrameWithCurrentText()
    }

    /// Update frame of the component, text input and update list icon accordingly.
    private func updateFrameWithCurrentText() {
        let origin = frame.origin
        let maxSize = CGSize(width: frame.width - TodoListView.iconSpace, height: CGFloat.greatestFiniteMagnitude)
        let height = expectedSelfHeight(in: maxSize) + TodoListView.headerHeight
        let newFrame = CGRect(origin: origin, size: CGSize(width: frame.width, height: height))

        if frame != newFrame {
            frame = newFrame
            viewModel.frame = newFrame
            headerView?.frame = calculateHeaderFrame()
            textInput?.frame = calculateEditorFrame()
            updateListTypeIcon()
        }
    }

    private func frameTextviewText() -> CGRect {
        let origin = frame.origin
        let maxSize = CGSize(width: frame.width - TodoListView.iconSpace, height: CGFloat.greatestFiniteMagnitude)
        let height = expectedSelfHeight(in: maxSize)
        return CGRect(origin: origin, size: CGSize(width: frame.width, height: height))
    }

    private func expectedSelfHeight(in size: CGSize) -> CGFloat {
        let newSize = textInput?.sizeThatFits(size)
        return (newSize?.height ?? 0)
    }

    /// Calculate editor frame inside its parent
    private func calculateEditorFrame() -> CGRect {
        let editorFrame = CGRect(x: TodoListView.iconSpace,
                                 y: TodoListView.headerHeight,
                                 width: frame.width - TodoListView.iconSpace,
                                 height: frame.height + 100 - TodoListView.headerHeight)
        // Leave some space for autogrowing input
        return editorFrame
    }

    private func calculateHeaderFrame() -> CGRect {
        return CGRect(x: 0, y: 0, width: frame.width, height: TodoListView.headerHeight)
    }

    @objc func tapGestureHandler(tapGesture: UITapGestureRecognizer) {
        guard !isResizingMode else {
            return
        }
        let location = tapGesture.location(in: textInput)
        if let range = textInput?.characterRange(at: location) {
            let cursorRange = textInput?.textRange(from: range.start, to: range.start)
            textInput?.selectedTextRange = cursorRange
        }
        startEditing()
    }

    /// Resign focus and disable user intraction
    func stopEditing() {
        headerView?.titleInput.resignFirstResponder()
        textInput?.resignFirstResponder()
        textInput?.isUserInteractionEnabled = false
        viewModel.text = textInput?.text
        viewModel.titleHeader = headerView?.titleInput.text
    }

    /// Become focus and enable user intraction
    func startEditing() {
        textInput?.becomeFirstResponder()
        textInput?.isUserInteractionEnabled = true
    }

    private func clearListIcons() {
        for view in listTypeIcons {
            view.removeFromSuperview()
        }
        listTypeIcons = []

        for view in signifierIcons {
            view.removeFromSuperview()
        }
        signifierIcons = []
    }

    private func renderListIcon(forParagraph index: Int, shouldAddSubview: Bool) {
        guard let textView = textInput,
            index >= 0,
            index < textView.paragraphRects.count,
            index < viewModel.paragraphTypes.count else {
            return
        }
        let paragraphRect = textView.paragraphRects[index]
        let listFrame = listTypeFrame(forParagraphRect: paragraphRect)
        let signifierFrame = listFrame.offsetBy(dx: -listFrame.width - 5, dy: 0)
        let listType = viewModel.paragraphTypes[index].type
        let listIcon = TodoListViewModel.listImageForType(listType)
        let signifierIcon = TodoListViewModel.signifierImage(for: listType.signifier)
        if shouldAddSubview {
            let listIconView = UIImageView(frame: listFrame)
            listIconView.contentMode = .scaleAspectFit
            listIconView.image = listIcon
            listIconView.tag = index
            listIconView.isUserInteractionEnabled = true
            let iconTapGesture = UITapGestureRecognizer(target: self, action: #selector(listIconTapped(gesture:)))
            listIconView.addGestureRecognizer(iconTapGesture)

            addSubview(listIconView)
            listTypeIcons.append(listIconView)

            let signifierIconView = UIImageView(frame: signifierFrame)
            signifierIconView.contentMode = .scaleAspectFit
            signifierIconView.image = signifierIcon
            addSubview(signifierIconView)
            signifierIcons.append(signifierIconView)
        } else {
            listTypeIcons[index].frame = listFrame
            listTypeIcons[index].image = listIcon

            signifierIcons[index].frame = signifierFrame
            signifierIcons[index].image = signifierIcon
        }
    }

    private func updateListTypeIcon() {
        guard let input = textInput else {
            return
        }
        var shouldAddSubview = false
        if input.paragraphRects.count != listTypeIcons.count || input.paragraphRects.isEmpty {
            clearListIcons()

            if input.paragraphRects.isEmpty {
                return
            }

            shouldAddSubview = true
        }
        for index in 0..<input.paragraphRects.count {
            renderListIcon(forParagraph: index, shouldAddSubview: shouldAddSubview)
        }
    }

    fileprivate func listTypeFrame(forParagraphRect rect: CGRect) -> CGRect {
        let relativeRect = convert(rect, from: textInput)
        let insetTop = textInput?.textContainerInset.top ?? 0
        let padding: CGFloat = 5.0
        let originX: CGFloat = listTypeIconSize.width + padding
        let originY: CGFloat = relativeRect.minY + 2 + insetTop
        let origin = CGPoint(x: originX, y: originY)
        return CGRect(origin: origin, size: listTypeIconSize)
    }

    fileprivate func setListTypeAtCurrentCursor(_ type: TodoListViewModel.ListType) {
        guard let paragraphIndex = textInput?.selectedParagraphIndex else {
            return
        }
        setListType(type, atParagraph: paragraphIndex)
    }
    fileprivate func setListType(_ type: TodoListViewModel.ListType, atParagraph paragraph: Int) {
        guard  paragraph < listTypeIcons.count,
            paragraph < viewModel.paragraphTypes.count else {
                return
        }
        // Update view model as well as image
        viewModel.paragraphTypes[paragraph].type = type
        listTypeIcons[paragraph].image = TodoListViewModel.listImageForType(type)
    }

    fileprivate func setSignifierAtCurrentCursor(_ signifier: TodoListViewModel.Signifier?) {
        guard let paragraphIndex = textInput?.selectedParagraphIndex,
            paragraphIndex < viewModel.paragraphTypes.count else {
                return
        }
        viewModel.updateSignifier(at: paragraphIndex, signifier: signifier)
        signifierIcons[paragraphIndex].image = TodoListViewModel.signifierImage(for: signifier)
    }

    /// Set attribute text for paragraph at specified index base on list state.
    ///
    /// This method will always set the attribute regardless of previous state.
    /// Please do the check before calling this method.
    /// - Parameter index: Paragraph Index
    fileprivate func updateTextAttribute(atParagraph index: Int) {
        guard index < viewModel.paragraphTypes.count,
            let ranges = textInput?.paragraphRanges,
            index < ranges.count,
            let state = viewModel.paragraphTypes[index].type.state,
            let attributedText = textInput?.attributedText else {
            return
        }
        let newAttributedText = NSMutableAttributedString(attributedString: attributedText)
        if state == .completed {
            newAttributedText.setAttributes(completedTextAttributes, range: ranges[index])
        } else {
            newAttributedText.setAttributes(textAttributes, range: ranges[index])
        }
        let selectedRange = textInput?.selectedRange
        textInput?.attributedText = newAttributedText
        if let previousSelectedRange = selectedRange {
            textInput?.selectedRange = previousSelectedRange
        }
    }

    fileprivate func updateAccessoryIconsForCurrentCursor() {
        guard let index = textInput?.selectedParagraphIndex, index < viewModel.paragraphTypes.count else {
            return
        }
        updateAccesoryIcons(atParagraph: index)
    }
    fileprivate func updateAccesoryIcons(atParagraph index: Int) {
        guard index >= 0, index < viewModel.paragraphTypes.count else {
            return
        }
        let types = TodoListViewModel.accessoryContextTypes(forType: viewModel.paragraphTypes[index].type)
        let inputAccessory = textInput?.inputAccessoryView as? ListInputAccessoryView
        inputAccessory?.setContextType(types: types, signifier: viewModel.paragraphTypes[index].type.signifier)
    }

    @objc func listIconTapped(gesture: UITapGestureRecognizer) {
        guard let icon = gesture.view else {
            return
        }
        let index = icon.tag
        guard index >= 0, index < viewModel.paragraphTypes.count else {
            return
        }
        let toggleType = ListInputAccessoryViewModel.toggleStateForType(viewModel.paragraphTypes[index].type)
        handleToggledType(toggleType, atParagraph: index)
    }
}

extension TodoListView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textDelegate?.textDidBeginEditing(listView: self)
    }
}

// MARK: - UITextViewDelegate
extension TodoListView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textDelegate?.textDidBeginEditing(listView: self)
    }

    func textViewDidChange(_ textView: UITextView) {
        let endPosition = textView.endOfDocument
        let currentRect = textView.caretRect(for: endPosition)
        let diff = currentRect.origin.y - previousRect.origin.y
        viewModel.text = textView.text
        if  diff != 0 {
            updateFrameWithCurrentText()
            textInput?.updateParagraphRects()
            updateListTypeIcon()
            for index in 0..<(textInput?.paragraphRects.count ?? 0) {
                updateTextAttribute(atParagraph: index)
            }
            previousRect = currentRect
        } else if listTypeIcons.count != viewModel.paragraphTypes.count {
            textInput?.updateParagraphRects()
            updateListTypeIcon()
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let listTypes = viewModel.paragraphTypes.compactMap { $0.type }
        do {
            viewModel.paragraphTypes = try viewModel.replaceTextInRange(range,
                                                                        with: text,
                                                                        paragraphText: textView.text,
                                                                        types: listTypes)
        } catch TodoListViewModel.ReplacingTextError.invalidArgument(message: _) {
            // No handle yet.
            return true
        } catch {
            // No handle yet.
            return true
        }
        guard viewModel.hasLinebreakChangeByReplacingTextInRange(range,
                                                                 with: text,
                                                                 in: textView.text) else {
                                                                    return true
        }
        let newCursorIndex = range.lowerBound + text.utf16.count
        let paragraphIndex = viewModel.paragraphTypes.firstIndex { paragraphType in
            return paragraphType.range.lowerBound <= newCursorIndex
                && paragraphType.range.upperBound >= newCursorIndex
        }
        guard let index = paragraphIndex else {
            return true
        }
        let isCompleted = viewModel.paragraphTypes[index].type.state == .completed
        textView.typingAttributes = isCompleted ? completedTextAttributes : textAttributes
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.text = textView.text
    }
}

// MARK: - ListInputAccessoryViewDelegate
extension TodoListView: ListInputAccessoryViewDelegate {
    func didTapButtonType(_ type: TodoListViewModel.ListType, in view: ListInputAccessoryView) {
        guard let index = textInput?.selectedParagraphIndex else {
            return
        }
        handleToggledType(type, atParagraph: index)
    }

    func handleToggledType(_ type: TodoListViewModel.ListType, atParagraph index: Int) {
        // update rects and ranges
        textInput?.updateParagraphRects()
        setListType(type, atParagraph: index)
        updateAccesoryIcons(atParagraph: index)
        updateTextAttribute(atParagraph: index)
    }

    func didTapSignifier(_ signifier: TodoListViewModel.Signifier?, in view: ListInputAccessoryView) {
        setSignifierAtCurrentCursor(signifier)
        updateAccessoryIconsForCurrentCursor()
    }
}

// MARK: - ListInputViewDelegate
extension TodoListView: ListInputViewDelegate {
    func paragraphRectDidChange(textView: ListInputView) {
        updateFrameWithCurrentText()
    }

    func selectedRangeDidChange(textView: ListInputView) {
        updateAccessoryIconsForCurrentCursor()
    }
}
