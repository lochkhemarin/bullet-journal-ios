//
//  TodoListViewModel.swift
//  Bujo
//
//  Created by Khemarin on 12/4/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation
import UIKit

class TodoListViewModel {
    /// Only for storing
    var text: String?
    var titleHeader: String?

    var identifier: String?
    var frame: CGRect = .zero

    /// Store list of `ParagraphType` when `textView(_,shouldChangeTextIn,replacementText)`
    var paragraphTypes: [ParagraphType] = []

    /// Get array `ParagraphType` by generating paragraph ranges from text combine with types provided.
    ///
    /// - Parameters:
    ///   - text: Text
    ///   - types: Existing ListType
    /// - Returns: ParagarphType
    func paragraphTypes(for text: String, types: [ListType]) -> [ParagraphType] {
        guard !text.isEmpty else {
            return []
        }
        var paragraphs: [ParagraphType] = []
        let string = text as NSString
        // Ranges should be used as the base.
        // Types more than total paragraphs should be ignored.
        // If less than total paragraphs, should use default empty type.
        let typeCount = types.count
        var index = 0
        string.enumerateParagraph { (string, range) in
            let type = index < typeCount ? types[index] : .none
            let paragraph = ParagraphType(range: range, type: type, text: string ?? "")
            paragraphs.append(paragraph)
            index += 1
        }

        return paragraphs
    }

    func replaceTextInRange(_ range: NSRange,
                            with text: String,
                            paragraphText: String,
                            types: [ListType]) throws -> [ParagraphType] {
        let paragraphNSString = paragraphText as NSString
        let paragraphRange = NSRange(location: 0, length: paragraphNSString.length)
        guard paragraphRange.lowerBound <= range.lowerBound,
            paragraphRange.upperBound >= range.upperBound else {
            throw ReplacingTextError.invalidArgument(message: "Invalid replacing range.")
        }
        let newParagraph = paragraphNSString.replacingCharacters(in: range, with: text)
        let existingTypes = paragraphTypes(for: paragraphText, types: types)
        var newTypes = paragraphTypes(for: newParagraph, types: [])

        let totalExistingElements = existingTypes.count
        let totalNewElements = newTypes.count
        let gap = totalExistingElements - totalNewElements
        var beforeMergedIndex = 0
        for (index, paragraph) in existingTypes.enumerated() {
            if paragraph.range.lowerBound < range.lowerBound {
                newTypes[index].type = paragraph.type
                beforeMergedIndex = index
            } else if paragraph.range.lowerBound >= range.upperBound {
                let newIndex = index - gap
                if newIndex >= 0, newIndex < totalNewElements, newIndex != beforeMergedIndex {
                    newTypes[newIndex].type = paragraph.type
                }
            }
        }
        return newTypes
    }

    func hasLinebreakChangeByReplacingTextInRange(_ range: NSRange,
                                                  with replacingText: String,
                                                  in text: String) -> Bool {
        let textRange = NSRange(location: 0, length: text.utf16.count)
        guard range.lowerBound >= textRange.lowerBound, range.upperBound <= textRange.upperBound else {
            return false
        }
        let oldText = (text as NSString).substring(with: range)
        return oldText.hasLinebreak() || text.hasLinebreak()
    }

    static func listImageForType(_ type: ListType) -> UIImage? {
        switch type {
        case .task(state: let state, signifier: _):
            return taskImage(state: state)
        case .event(state: let state, signifier: _):
            return eventImage(state: state)
        case .note(state: _):
            return noteImage()
        case .none:
            return nil
        }
    }

    static func signifierImage(for signifier: Signifier?) -> UIImage? {
        guard let signifier = signifier else {
            return nil
        }
        switch signifier {
        case .important:
            return Asset.star.image
        case .inspiration:
            return Asset.crown.image
        }
    }

    private static func taskImage(state: ListState) -> UIImage? {
        switch state {
        case .completed:
            return Asset.taskCompleted.image
        case .normal, .canceled:
            return Asset.taskEmpty.image
        }
    }

    private static func eventImage(state: ListState) -> UIImage? {
        switch state {
        case .completed:
            return Asset.eventCompleted.image
        case .normal, .canceled:
            return Asset.eventEmpty.image
        }
    }

    private static func noteImage() -> UIImage? {
        return Asset.note.image
    }

    /// Calculate array of ListType for input accessory.
    ///
    /// Currently only implement task. Other are to be added
    ///
    /// - Parameter type: current list type for the paragraph.
    /// - Returns: Array of toggled list type
    static func accessoryContextTypes(forType type: ListType) -> [ListType] {
        switch type {
        case .none:
            return [
                ListType.note(state: .normal, signifier: nil),
                ListType.event(state: .normal, signifier: nil),
                ListType.task(state: .normal, signifier: nil)
            ]
        case .event(state: _):
            return [
                ListType.note(state: .normal, signifier: nil),
                ListInputAccessoryViewModel.toggleStateForType(type),
                ListType.task(state: .normal, signifier: nil)
            ]
        case .note(state: _):
            return [
                ListInputAccessoryViewModel.toggleStateForType(type),
                ListType.event(state: .normal, signifier: nil),
                ListType.task(state: .normal, signifier: nil)
            ]
        case .task(state: _):
            return [
                ListType.note(state: .normal, signifier: nil),
                ListType.event(state: .normal, signifier: nil),
                ListInputAccessoryViewModel.toggleStateForType(type)
            ]
        }
    }

    func updateSignifier(at index: Int, signifier: Signifier?) {
        guard index >= 0, index < paragraphTypes.count else {
            return
        }
        let oldType = paragraphTypes[index]
        let typeRawValue = oldType.type.rawValue
        let typeComponents = typeRawValue.components(separatedBy: ".")
        if typeComponents.count == 3 {
            let newComponents: [String] = [
                typeComponents[0],
                typeComponents[1],
                "\(signifier?.rawValue ?? -1)"
            ]
            if let newType = ListType(rawValue: newComponents.joined(separator: ".")) {
                paragraphTypes[index] = ParagraphType(range: oldType.range,
                                                      type: newType,
                                                      text: oldType.text)
            }
        }
    }
}

extension TodoListViewModel {
    enum ReplacingTextError: Error {
        case invalidArgument(message: String)
    }

    /// State of input button indicating list state.
    ///
    /// Migrated and other state might be added in the future.
    ///
    /// - normal: Normal state
    /// - completed: Completed state
    /// - canceled: Canceled state
    enum ListState: Int {
        case normal
        case completed
        case canceled
    }

    enum Signifier: Int {
        case inspiration
        case important
    }

    enum ListType: RawRepresentable, Equatable {
        typealias RawValue = String

        init?(rawValue: String) {
            let components = rawValue.components(separatedBy: ".")
            guard components.count == 3 else {
                return nil
            }
            let state: TodoListViewModel.ListState
            if let stateInt = Int(components[1]), let initState = TodoListViewModel.ListState(rawValue: stateInt) {
                state = initState
            } else {
                state = TodoListViewModel.ListState.normal
            }
            var signifier: TodoListViewModel.Signifier?
            if let signifierInt = Int(components[2]) {
                signifier = TodoListViewModel.Signifier(rawValue: signifierInt)
            }
            switch components[0] {
            case "task":
                self = .task(state: state, signifier: signifier)
            case "event":
                self = .event(state: state, signifier: signifier)
            case "note":
                self = .note(state: state, signifier: signifier)
            case "none":
                self = .none
            default:
                return nil
            }
        }

        var rawValue: String {
            switch self {
            case .task(state: let state, signifier: let signifier):
                return "task.\(state.rawValue).\(signifier?.rawValue ?? -1)"
            case .event(state: let state, signifier: let signifier):
                return "event.\(state.rawValue).\(signifier?.rawValue ?? -1)"
            case .note(state: let state, signifier: let signifier):
                return "note.\(state.rawValue).\(signifier?.rawValue ?? -1)"
            case .none:
                return "none.0.-1"
            }
        }

        case task(state: ListState, signifier: Signifier?)
        case event(state: ListState, signifier: Signifier?)
        case note(state: ListState, signifier: Signifier?) // Note should only have normal and canceled state?
        case none

        var state: ListState? {
            switch self {
            case .task(state: let state, signifier: _):
                return state
            case .event(state: let state, signifier: _):
                return state
            case .note(state: let state, signifier: _):
                return state
            case .none:
                return nil
            }
        }

        var signifier: Signifier? {
            switch self {
            case .task(state: _, signifier: let signifier):
                return signifier
            case .event(state: _, signifier: let signifier):
                return signifier
            case .note(state: _, signifier: let signifier):
                return signifier
            case .none:
                return nil
            }
        }

        static func == (lhs: ListType, rhs: ListType) -> Bool {
            return lhs.rawValue == rhs.rawValue
        }
    }

    struct ParagraphType {
        var range: NSRange
        var type: ListType = .none
        var text: String
    }
}
