//
//  SimpleListHeader.swift
//  Bujo
//
//  Created by Khemarin on 11/17/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class SimpleListHeader: UIView {
    @IBOutlet weak var titleInput: UITextField!

    var text: String? {
        didSet {
            titleInput?.text = text
        }
    }
}
