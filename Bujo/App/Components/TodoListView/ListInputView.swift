//
//  ListInputView.swift
//  Bujo
//
//  Created by Khemarin on 10/31/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

protocol ListInputViewDelegate: class {
    func selectedRangeDidChange(textView: ListInputView)
    func paragraphRectDidChange(textView: ListInputView)
}

class ListInputView: UITextView {
    weak var listDelegate: ListInputViewDelegate?

    var paragraphRects: [CGRect] = [] {
        didSet {
            if paragraphRects != oldValue {
                listDelegate?.paragraphRectDidChange(textView: self)
            }
        }
    }

    var paragraphRanges: [NSRange] = []

    /// Calculate selected rectangle base on `selectedRange`
    var selectedRect: CGRect {
        let glyphRange = layoutManager.glyphRange(forCharacterRange: selectedRange, actualCharacterRange: nil)
        return layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
    }

    /// Value is calculated by `firstIndex` of `paragraphRects`
    var selectedParagraphIndex: Int? {
        return paragraphRects.firstIndex(where: {$0.contains(selectedRect)})
    }

    override var selectedTextRange: UITextRange? {
        didSet {
            listDelegate?.selectedRangeDidChange(textView: self)
        }
    }

    override var text: String! {
        didSet {
            updateParagraphRects()
        }
    }

    override var attributedText: NSAttributedString! {
        didSet {
            updateParagraphRects()
        }
    }

    func paragraphsInRange(_ range: NSRange) -> [Int] {
        var indices: [Int] = []
        for (index, paragraphRange) in paragraphRanges.enumerated() {
            if range.contains(paragraphRange.upperBound) {
                indices.append(index)
            }
        }
        return indices
    }

    func updateParagraphRects() {
        var newRects: [CGRect] = []
        paragraphRanges = []
        guard !text.isEmpty else {
            return
        }
        let string = text as NSString
        string.enumerateParagraph(using: { [weak self] (_, paragraphRange) in
            guard let strongSelf = self else {
                return
            }
            let glyphRange = strongSelf.layoutManager.glyphRange(forCharacterRange: paragraphRange,
                                                                 actualCharacterRange: nil)
            let rect = strongSelf.layoutManager.boundingRect(forGlyphRange: glyphRange, in: strongSelf.textContainer)
            let size = CGSize(width: strongSelf.frame.width, height: rect.height)
            let fullWidthRect = CGRect(origin: rect.origin, size: size)
            newRects.append(fullWidthRect)
            strongSelf.paragraphRanges.append(paragraphRange)
        })
        paragraphRects = newRects
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateParagraphRects()
    }
}

extension ListInputView {
    enum ParagraphReplaceLocation {
        case lead
        case trail
        case middle
        case all
    }
    enum ParagraphChangeStatus {
        case remain
        case replace(location: ParagraphReplaceLocation)
        case added
        case deleted
    }
    struct ParagraphChange {
        let oldIndex: Int?
        let index: Int?
        let status: ParagraphChangeStatus
    }
}
