//
//  Coordinator.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

protocol Coordinator: class {
    var identifer: UUID { get set }
    var childCoordinator: [UUID: Coordinator] { get set }
    
    func start()
    func end()

    func addChildCoordinator(_ coordinator: Coordinator)
    func removeChildCoordinator(_ coordinator: Coordinator)
}
