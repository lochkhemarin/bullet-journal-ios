//
//  UIViewExtension.swift
//  Bujo
//
//  Created by Khemarin on 10/25/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

extension UIView {
    @discardableResult
    func fromNib() -> UIView? {
        let nibName = String(describing: type(of: self))
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        guard let contentView = nibView else {
            return nil
        }
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return contentView
    }

    class func fromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?.first as? T
    }

//    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard isInViewController() else {
//            return
//        }
//        next?.touchesEnded(touches, with: event)
//    }

//    private func isInViewController() -> Bool {
//        guard let superView = superview else {
//            return false
//        }
//        return String(describing: superView.classForCoder) == "UIViewControllerWrapperView"
//    }
}
