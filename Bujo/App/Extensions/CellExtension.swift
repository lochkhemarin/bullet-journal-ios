//
//  CellExtension.swift
//  Bujo
//
//  Created by Khemarin on 10/21/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    static var cellIdentifier: String {
        return String(describing: self)
    }

    static var nibName: String {
        return String(describing: self)
    }
}
