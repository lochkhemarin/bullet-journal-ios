//
//  DateExtension.swift
//  Bujo
//
//  Created by Khemarin on 10/14/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

extension Date {
    var month: Month {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self)
        let month = calendar.component(.month, from: self)
        return Month(year: year, month: month)
    }

    func firstDayOfMonth() -> Date? {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month], from: self)
        components.day = 1
        return calendar.date(from: components)
    }

    func lastDayOfMonth() -> Date? {
        let calendar = Calendar.current
        	var components = calendar.dateComponents([.year, .month], from: self)
        guard let upperBound = calendar.range(of: .day, in: .month, for: self)?.upperBound else {
            return nil
        }
        components.day = upperBound - 1
        return calendar.date(from: components)
    }

    func stringForTodoListTitle() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
}
