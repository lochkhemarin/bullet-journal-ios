//
//  StringExtension.swift
//  Bujo
//
//  Created by Khemarin on 12/9/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

extension String {
    /// Check if string containing line break.
    ///
    /// - Returns: True if contains linebreak
    func hasLinebreak() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.newlines) != nil
    }

    /// Check if string has linebreak at the end
    ///
    /// - Returns: True if it is end with linebreak
    func isEndWithLinebreak() -> Bool {
        let string = self as NSString
        let length = string.length
        if let lastCharacter = Unicode.Scalar(string.character(at: length - 1)),
            CharacterSet.newlines.contains(lastCharacter) {
            return true
        }
        return false
    }
}

extension NSString {
    func enumerateParagraph(using block: @escaping (String?, NSRange) -> Void) {
        let range = NSRange(location: 0, length: length)
        enumerateSubstrings(in: range, options: .byParagraphs) { (paragraph, paragraphRange, _, _) in
            block(paragraph, paragraphRange)
        }
        if (self as String).isEndWithLinebreak() {
            block("", NSRange(location: length, length: 0))
        }
    }
}
