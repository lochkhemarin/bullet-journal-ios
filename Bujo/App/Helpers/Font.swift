//
//  Font.swift
//  Bujo
//
//  Created by Khemarin on 10/20/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

enum Font {
    /// Get default script font
    ///
    /// - Parameter size: Font size
    /// - Returns: UIFont
    static func scriptFont(ofSize size: CGFloat) -> UIFont {
        // Family: Fall is Coming, Font names: ["FallisComing-Regular"]
        // Family: cookies&milk, Font names: ["cookies&milk-regular"]
        // Family: Warung Kopi Light, Font names: ["WarungKopiLight"]
        guard let scriptFont = UIFont(name: "WarungKopiLight", size: size) else {
            fatalError("""
        Failed to load the "WarungKopiLight" font.
        Make sure the font file is included in the project and the font name is spelled correctly.
        """
            )
        }
        return UIFontMetrics.default.scaledFont(for: scriptFont)
    }

    /// Get default script font in BOLD
    ///
    /// - Parameter size: Font size
    /// - Returns: UIFont
    static func boldScriptFont(ofSize size: CGFloat) -> UIFont {
        // Family: Warung Kopi Bold, Font names: ["WarungKopi-Bold"]
        guard let scriptFont = UIFont(name: "WarungKopi-Bold", size: size) else {
            fatalError("""
        Failed to load the "WarungKopi-Bold" font.
        Make sure the font file is included in the project and the font name is spelled correctly.
        """
            )
        }
        return UIFontMetrics.default.scaledFont(for: scriptFont)
    }
}
