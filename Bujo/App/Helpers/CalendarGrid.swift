//
//  CalendarGrid.swift
//  Bujo
//
//  Created by Khemarin on 10/14/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

class CalendarGrid {
    lazy var calendar: Calendar = {
        var calendar = Calendar.current
        calendar.locale = Locale.autoupdatingCurrent
        return calendar
    }()

    private let month: Month
    var useShortWeekday = false

    var weekdaySymbols: [String] {
        if useShortWeekday {
            return calendar.shortWeekdaySymbols
        }
        return calendar.weekdaySymbols
    }

    lazy var grid: [[Int]]? = {
        var components = DateComponents(year: month.year, month: month.month)
        components.calendar = calendar
        components.timeZone = calendar.timeZone

        guard components.isValidDate, let firstDay = calendar.date(from: components) else {
            return nil
        }

        // Range of weekday is 1-7
        let firstDayWeekday = calendar.component(.weekday, from: firstDay)

        guard let days = calendar.range(of: .day, in: .month, for: firstDay)?.count else {
            return nil
        }

        var daysBeforeStart = firstDayWeekday - calendar.firstWeekday
        if daysBeforeStart < 0 { // Shift
            daysBeforeStart = 7 + daysBeforeStart
        }
        let weeks = Int(ceil(Float(days + daysBeforeStart) / 7))

        var date = -daysBeforeStart + 1 // zero exclude
        var grid: [[Int]] = []
        for _ in 0..<weeks {
            var daysInWeek: [Int] = []
            for _ in 0..<7 {
                var currentDate = date
                if currentDate < 1 || currentDate > days {
                    currentDate = 0
                }
                daysInWeek.append(currentDate)
                date += 1
            }
            grid.append(daysInWeek)
        }

        return grid
    }()

    var days: Int {
        guard let grid = grid else {
            return 0
        }
        let days = grid.reduce(0) { $0 + ($1.compactMap { $0 == 0 ? nil : $0 }).count }
        return days
    }

    init(baseMonth month: Month) {
        self.month = month
    }
}
