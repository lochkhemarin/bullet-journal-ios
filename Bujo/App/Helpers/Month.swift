//
//  Month.swift
//  Bujo
//
//  Created by Khemarin on 10/14/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation

struct Month: Equatable {
    let year: Int
    let month: Int

    var firstDayDate: Date? {
        let calendar = Calendar.current
        var components = DateComponents(year: year, month: month)
        components.calendar = calendar
        components.timeZone = calendar.timeZone

        guard components.isValidDate else {
            return nil
        }
        return calendar.date(from: components)
    }

    static func monthName(of monthOrder: Int) -> String {
        guard monthOrder > 0, monthOrder <= 12 else {
            return ""
        }
        return Calendar.current.monthSymbols[monthOrder - 1]
    }
}

func ==(lhs: Month, rhs: Month) -> Bool {
    return lhs.month == rhs.month && lhs.year == rhs.year
}
