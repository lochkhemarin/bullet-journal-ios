//
//  BaseViewController.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    private var backgroundImageView: UIImageView?

    internal var resizingView: MovableView?
    internal var editingView: TodoListView?

    var theme = BujoTheme.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView?.backgroundColor = theme.color.background
    }

//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let touch = touches.first else {
//            return
//        }
//        cancelEditingIfNeeded(touch: touch)
//        cancelResizingIfNeeded(touch: touch)
//    }

    private func cancelEditingIfNeeded(touch: UITouch) {
        guard let editingView = editingView else {
            return
        }
        let location = touch.location(in: editingView)
        if !editingView.bounds.contains(location) {
            editingView.stopEditing()
            self.editingView = nil
        }
    }

    private func cancelResizingIfNeeded(touch: UITouch) {
        guard let resizingView = resizingView else {
            return
        }
        let location = touch.location(in: resizingView)
        if !resizingView.bounds.contains(location) {
            resizingView.isResizingMode = false
            self.resizingView = nil
        }
    }
}
