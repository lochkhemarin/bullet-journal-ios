//
//  BaseCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 2/11/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class BaseCoordinator: Coordinator {
    var parentCoordinator: Coordinator?

    var identifer: UUID = UUID()
    var childCoordinator: [UUID: Coordinator] = [:]

    func addChildCoordinator(_ coordinator: Coordinator) {
        childCoordinator[coordinator.identifer] = coordinator
        (coordinator as? BaseCoordinator)?.parentCoordinator = self
    }

    func removeChildCoordinator(_ coordinator: Coordinator) {
        childCoordinator[coordinator.identifer] = nil
    }

    func start() {
        fatalError("Start method should be implemented in child coordinator.")
    }
    
    func end() {
        parentCoordinator?.removeChildCoordinator(self)
    }
}
