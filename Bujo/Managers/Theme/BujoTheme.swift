//
//  BujoTheme.swift
//  Bujo
//
//  Created by Khemarin on 7/2/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class AssetColor: ColorScheme {
    var primary: UIColor = Asset.primaryColor.color
    var secondary: UIColor = Asset.secondaryColor.color
    var primaryVariant: UIColor = Asset.primaryVariantColor.color
    var secondaryVariant: UIColor = Asset.secondaryVariantColor.color
    var background: UIColor = Asset.backgroundColor.color
    var surface: UIColor = Asset.surfaceColor.color
    var error: UIColor = Asset.errorColor.color
    var onPrimary: UIColor = Asset.onPrimaryColor.color
    var onSecondary: UIColor = Asset.onSecondaryColor.color
    var onBackground: UIColor = Asset.onBackgroundColor.color
    var onSurface: UIColor = Asset.onSurfaceColor.color
    var onError: UIColor = Asset.onErrorColor.color
}

class BujoTypography: Typography {
    var heading1: UIFont = Font.scriptFont(ofSize: 96)
    var heading2: UIFont = Font.scriptFont(ofSize: 60)
    var heading3: UIFont = Font.boldScriptFont(ofSize: 48) // semi
    var heading4: UIFont = Font.boldScriptFont(ofSize: 34) // semi
    var heading5: UIFont = Font.boldScriptFont(ofSize: 24) // semi
    var heading6: UIFont = Font.boldScriptFont(ofSize: 20) // semi
    var subtitle1: UIFont = Font.scriptFont(ofSize: 16) // medium
    var subtitle2: UIFont = Font.boldScriptFont(ofSize: 14) // semi
    var body1: UIFont = Font.scriptFont(ofSize: 14) // semi-bold
    var body2: UIFont = Font.scriptFont(ofSize: 16)
    var button: UIFont = Font.scriptFont(ofSize: 25) // semi-bold
    var caption: UIFont = Font.boldScriptFont(ofSize: 12) // medium
    var overline: UIFont = Font.scriptFont(ofSize: 12) // semi-bold
}

class BujoTheme {
    static let shared = BujoTheme()

    let color = AssetColor()
    let font = BujoTypography()
}
