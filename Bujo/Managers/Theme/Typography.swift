//
//  Typography.swift
//  Bujo
//
//  Created by Khemarin on 7/2/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

protocol Typography {
    var heading1: UIFont { get set }
    var heading2: UIFont { get set }
    var heading3: UIFont { get set }
    var heading4: UIFont { get set }
    var heading5: UIFont { get set }
    var heading6: UIFont { get set }
    var subtitle1: UIFont { get set }
    var subtitle2: UIFont { get set }
    var body1: UIFont { get set }
    var body2: UIFont { get set }
    var button: UIFont { get set }
    var caption: UIFont { get set }
    var overline: UIFont { get set }
}
