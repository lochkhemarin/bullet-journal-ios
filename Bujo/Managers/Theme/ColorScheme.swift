//
//  ColorScheme.swift
//  Bujo
//
//  Created by Khemarin on 7/2/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

protocol ColorScheme {
    var primary: UIColor { get set }
    var secondary: UIColor { get set }
    var primaryVariant: UIColor { get set }
    var secondaryVariant: UIColor { get set }

    var background: UIColor { get set }
    var surface: UIColor { get set }
    var error: UIColor { get set }

    var onPrimary: UIColor { get set }
    var onSecondary: UIColor { get set }
    var onBackground: UIColor { get set }
    var onSurface: UIColor { get set }
    var onError: UIColor { get set }
}
