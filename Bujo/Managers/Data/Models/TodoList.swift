//
//  TodoList.swift
//  Bujo
//
//  Created by Khemarin on 11/19/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation
import RealmSwift

class TodoList: Object {
    @objc dynamic var id = UUID().uuidString
    /// Title of todo list
    @objc dynamic var title: String = ""
    /// Tasks in the list
    @objc dynamic var list: String?
    @objc dynamic var frame: String?

    @objc dynamic var date: Date = Date()

    override static func primaryKey() -> String? {
        return "id"
    }
}

class Frame: Codable {
    var x: Float = 0
    var y: Float = 0
    var width: Float = 0
    var height: Float = 0

    var asCGRect: CGRect {
        return CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(width), height: CGFloat(height))
    }

    init(x: Float, y: Float, width: Float, height: Float) {
        self.x = x
        self.y = y
        self.width = width
        self.height = height
    }

    enum CodingKeys: String, CodingKey {
        case x
        case y
        case width
        case height
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(x, forKey: .x)
        try container.encode(y, forKey: .y)
        try container.encode(width, forKey: .width)
        try container.encode(height, forKey: .height)
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        x = try values.decode(Float.self, forKey: .x)
        y = try values.decode(Float.self, forKey: .y)
        width = try values.decode(Float.self, forKey: .width)
        height = try values.decode(Float.self, forKey: .height)
    }
}

class Todo: Codable {
    /// List type.
    /// Empty: List type rawValue
    var listType: String = ""

    /// Task description
    var text: String = ""

    enum CodingKeys: String, CodingKey {
        case listType
        case text
    }

    init(listType: String, text: String) {
        self.listType = listType
        self.text = text
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(listType, forKey: .listType)
        try container.encode(text, forKey: .text)
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        listType = try values.decode(String.self, forKey: .listType)
        text = try values.decode(String.self, forKey: .text)
    }
}
