//
//  CalendarTrackerModel.swift
//  Bujo
//
//  Created by Khemarin on 1/2/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import RealmSwift

class CalendarTrackerModel: Object {
    @objc dynamic var id = UUID().uuidString
    /// Title of todo list
    @objc dynamic var title: String = ""
    /// Selected day: "1,3,4,5"
    @objc dynamic var selectedDay: String = ""
    @objc dynamic var date: Date = Date()

    override static func primaryKey() -> String? {
        return "id"
    }
}
