//
//  DataManager.swift
//  Bujo
//
//  Created by Khemarin on 11/20/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation
import RealmSwift

class DataManager {
    static let shared = DataManager()

    let localStorage: LocalStorage

    private init() {
        localStorage = LocalStorage()
    }
}
