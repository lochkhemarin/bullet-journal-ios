//
//  LocalStorage.swift
//  Bujo
//
//  Created by Khemarin on 11/19/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation
import RealmSwift
import Valet

class LocalStorage {
    fileprivate var realm: Realm?
    private var identifierKey = "com.vemean.taskjournal"
    private var storeKey = "com.vemean.taskjournal.key"

    private let concurrentQueue = DispatchQueue.global(qos: .background)

    init() {
        configure()
    }

    func deleteList(_ viewModel: TodoListViewModel, completion: ((SaveResponse) -> Void)?) {
        if let identifier = viewModel.identifier,
            let todo = realm?.object(ofType: TodoList.self, forPrimaryKey: identifier) {
            guard let realm = realm else {
                let errorInfo: [String: String] = [
                    ErrorInfo.message.rawValue: "Db is not instantiated."
                ]
                completion?(.failed(error: NSError(domain: "", code: 0, userInfo: errorInfo)))
                return
            }

            do {
                try realm.write {
                    realm.delete(todo)
                    completion?(.success(id: identifier))
                }
            } catch let error as NSError {
                completion?(.failed(error: error))
            }
        } else {
            let errorInfo: [String: String] = [
                ErrorInfo.message.rawValue: "Could not delete object"
            ]
            completion?(.failed(error: NSError(domain: "", code: 0, userInfo: errorInfo)))
        }
    }

    func saveList(_ viewModel: TodoListViewModel, toMonthOfDate date: Date, completion: ((SaveResponse) -> Void)?) {
        let paragraphs = viewModel.paragraphTypes
        let listCount = paragraphs.count
        guard let realm = realm else {
            let errorInfo: [String: String] = [
                ErrorInfo.message.rawValue: "Db is not instantiated."
            ]
            completion?(.failed(error: NSError(domain: "", code: 0, userInfo: errorInfo)))
            return
        }
        do {
            try realm.write {
                var tasks: [Todo] = []
                for index in 0..<listCount {
                    let task = Todo(listType: paragraphs[index].type.rawValue, text: paragraphs[index].text)
                    tasks.append(task)
                }
                var tasksString: String?
                if let encodedTasks = try? JSONEncoder().encode(tasks) {
                    tasksString = String(data: encodedTasks, encoding: .utf8)
                }
                var listValues: [String: Any?] = [
                    "list": tasksString,
                    "title": viewModel.titleHeader ?? "",
                    "date": date,
                    "frame": createFrame(cgRect: viewModel.frame)
                ]

                if let identifier = viewModel.identifier {
                    listValues["id"] = identifier
                    realm.create(TodoList.self, value: listValues, update: true)
                    completion?(.success(id: identifier))
                } else {
                    let listObject = realm.create(TodoList.self, value: listValues)
                    realm.add(listObject)
                    completion?(.success(id: listObject.id))
                }
            }
        } catch let error as NSError {
            completion?(.failed(error: error))
        }
    }

    func getTodoListFromMonth(of date: Date,
                     completion: (([TodoList]) -> Void)?) {
        guard let predicate = datePredicateInMonthOf(date: date) else {
            completion?([])
            return
        }
        guard let results = realm?.objects(TodoList.self).filter(predicate) else {
            completion?([])
            return
        }

        var list: [TodoList] = []
        for todoList in results {
            list.append(todoList)
        }
        completion?(list)
    }

    func getTodoListFromDate(_ date: Date,
                              completion: ((TodoList?) -> Void)?) {
        let predicate = NSPredicate(format: "date = %@", date as NSDate)
        guard let results = realm?.objects(TodoList.self).filter(predicate) else {
            completion?(nil)
            return
        }
        completion?(results.first)
    }

    func getCalendarTracker(from date: Date, completion: ([CalendarTrackerModel]) -> Void) {
        guard let predicate = datePredicateInMonthOf(date: date) else {
            completion([])
            return
        }
        guard let results = realm?.objects(CalendarTrackerModel.self).filter(predicate) else {
            completion([])
            return
        }
        var trackers: [CalendarTrackerModel] = []
        for tracker in results {
            trackers.append(tracker)
        }

        completion(trackers)
    }

    func saveCalendarTracker(viewModel: CalendarTrackerViewModel, completion: ((SaveResponse) -> Void)?) {
        guard let realm = realm else {
            let errorInfo: [String: String] = [
                ErrorInfo.message.rawValue: "Db is not instantiated."
            ]
            completion?(.failed(error: NSError(domain: "", code: 0, userInfo: errorInfo)))
            return
        }
        do {
            try realm.write {
                let model = viewModel.asModel
                var properties: [String: Any?] = [
                    "title": model?.title,
                    "selectedDay": model?.selectedDay,
                    "date": model?.date
                ]
                if let identifier = viewModel.identifier { // Edit
                    properties["id"] = identifier
                    realm.create(CalendarTrackerModel.self, value: properties, update: true)
                    completion?(.success(id: identifier))
                } else { // Create
                    let modelObject = realm.create(CalendarTrackerModel.self, value: properties)
                    realm.add(modelObject)
                    completion?(.success(id: modelObject.id))
                }
            }
        } catch let error as NSError {
            completion?(.failed(error: error))
        }
    }

    func deleteTracker(viewModel: CalendarTrackerViewModel, completion: ((SaveResponse) -> Void)?) {
        if let identifier = viewModel.identifier,
            let todo = realm?.object(ofType: CalendarTrackerModel.self, forPrimaryKey: identifier) {

            guard let realm = realm else {
                let errorInfo: [String: String] = [
                    ErrorInfo.message.rawValue: "Db is not instantiated."
                ]
                completion?(.failed(error: NSError(domain: "", code: 0, userInfo: errorInfo)))
                return
            }

            do {
                try realm.write {
                    realm.delete(todo)
                    completion?(.success(id: identifier))
                }
            } catch let error as NSError {
                completion?(.failed(error: error))
            }
        } else {
            let errorInfo: [String: String] = [
                ErrorInfo.message.rawValue: "Could not delete object"
            ]
            completion?(.failed(error: NSError(domain: "", code: 0, userInfo: errorInfo)))
        }
    }

    fileprivate func datePredicateInMonthOf(date: Date) -> NSPredicate? {
        guard let first = date.firstDayOfMonth() as NSDate?,
            let last = date.lastDayOfMonth() as NSDate? else {
                return nil
        }
        return NSPredicate(format: "date >= %@ && date <= %@", first, last)
    }

    private func configure() {
        #if targetEnvironment(simulator)
        let config = Realm.Configuration.defaultConfiguration
        #else
        let config = Realm.Configuration(encryptionKey: getKey())
        #endif
        do {
            realm = try Realm(configuration: config)
            print("realm: \(config.fileURL)")
        } catch let error as NSError {
            fatalError("Error opening realm: \(error)")
        }
    }

    private func getKey() -> Data {
        guard let identifier = Identifier(nonEmpty: identifierKey) else {
            fatalError("Can't instantiate identifier for Valet")
        }
        let valet = Valet.valet(with: identifier, accessibility: .whenUnlocked)
        if let key = valet.object(forKey: storeKey) {
            return key
        } else {
            var key = Data(count: 64)
            _ = key.withUnsafeMutableBytes { bytes in
                SecRandomCopyBytes(kSecRandomDefault, 64, bytes)
            }
            valet.set(object: key, forKey: storeKey)
            return key
        }

    }
}

// MARK: - Type
extension LocalStorage {
    enum SaveResponse {
        case success(id: String)
        case failed(error: NSError)
    }

    enum ErrorInfo: String {
        case message
    }
}

// MARK: - Helper
extension LocalStorage {
    fileprivate func createFrame(cgRect rect: CGRect) -> String? {
        let frame = Frame(x: Float(rect.origin.x),
                     y: Float(rect.origin.y),
                     width: Float(rect.width),
                     height: Float(rect.height))
        guard let encoded = try? JSONEncoder().encode(frame) else {
            return nil
        }
        return String(data: encoded, encoding: .utf8)
    }

    static func createTodoViewModel(from todoList: TodoList,
                                         defaultType: TodoListViewModel.ListType) -> TodoListViewModel {
        let viewModel = TodoListViewModel()
        viewModel.titleHeader = todoList.title
        if let frameString = todoList.frame, let frameData = frameString.data(using: .utf8) {
            do {
                let frame = try JSONDecoder().decode(Frame.self, from: frameData)
                viewModel.frame = frame.asCGRect
            } catch {
                print("error: \(error.localizedDescription)")
            }
        }
        var types: [TodoListViewModel.ListType] = []
        var paragraphs: [String] = []
        if let listString = todoList.list,
            let listData = listString.data(using: .utf8),
            let todos = try? JSONDecoder().decode([Todo].self, from: listData) {
            for todo in todos {
                let type = TodoListViewModel.ListType(rawValue: todo.listType) ?? defaultType
                let description = todo.text

                types.append(type)
                paragraphs.append(description)
            }
        }
        viewModel.identifier = todoList.id
        viewModel.text = paragraphs.joined(separator: "\n")
        viewModel.paragraphTypes = viewModel.paragraphTypes(for: viewModel.text ?? "", types: types)
        return viewModel
    }
}
