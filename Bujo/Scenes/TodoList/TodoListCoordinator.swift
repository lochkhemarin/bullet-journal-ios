//
//  TodoListCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 4/13/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class TodoListCoordinator: BaseCoordinator {
    typealias PresentableType = UIViewController & InteractivePresentor & InteractiveControllerDelegate
    let presentor: PresentableType
    let viewController: TodoListViewController
    weak var viewModel: TodoListScreenViewModel?
    let maskView: UIView

    init(presentor: PresentableType,
         controller: TodoListViewController,
         date: Date) {
        self.presentor = presentor
        viewController = controller
        viewController.viewModel = TodoListScreenViewModel(date: date)
        viewModel = viewController.viewModel

        maskView = UIView()
        maskView.backgroundColor = #colorLiteral(red: 0.5704585314, green: 0.5704723597, blue: 0.5704649091, alpha: 1)
        maskView.alpha = 0.5
    }

    override func start() {
        viewController.coordinator = self
        if let parentView = presentor.view {
            parentView.addSubview(maskView)
            maskView.frame = parentView.bounds
        }
        viewController.delegate = presentor
        presentor.addInteractiveController(viewController, percentageSteps: [0.0, 0.9])
        viewController.animateToStep(1)
    }

    override func end() {
        viewModel?.save(todoList: viewController.listView?.viewModel, completion: {
        })
        maskView.removeFromSuperview()
        viewController.listView?.textInput?.resignFirstResponder()
        viewController.view?.removeFromSuperview()
        viewController.removeFromParent()
        super.end()
    }
}
