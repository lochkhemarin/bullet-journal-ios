//
//  TodoListScreenViewModel.swift
//  Bujo
//
//  Created by Khemarin on 4/13/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import Foundation

class TodoListScreenViewModel {
    var date = Date()

    var title: String {
        return date.stringForTodoListTitle()
    }

    private var todoListModel: TodoList? {
        didSet {
            if let model = todoListModel {
                todoList = LocalStorage.createTodoViewModel(from: model, defaultType: .none)
            }
        }
    }
    private(set) var todoList: TodoListViewModel?

    init(date: Date) {
        self.date = date
    }

    func save(todoList: TodoListViewModel?, completion: @escaping () -> Void) {
        guard let viewModel = todoList else {
            completion()
            return
        }
        DataManager.shared.localStorage.saveList(viewModel, toMonthOfDate: date) { _ in
            completion()
        }
    }

    func getData(completion: @escaping () -> Void) {
        DataManager.shared.localStorage.getTodoListFromDate(date) { [weak self] result in
            self?.todoListModel = result
            completion()
        }
    }
}
