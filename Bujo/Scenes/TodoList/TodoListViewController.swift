//
//  TodoListViewController.swift
//  Bujo
//
//  Created by Khemarin on 3/10/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class TodoListViewController: InteractiveController {
    var listView: TodoListView?
    var viewModel: TodoListScreenViewModel?
    weak var coordinator: TodoListCoordinator?
    let theme = BujoTheme.shared

    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = viewModel?.title
        titleLabel.font = theme.font.heading6
        setupInput()

        viewModel?.getData(completion: { [weak self] in
            self?.renderInput()
        })
    }

    private func renderInput() {
        listView?.setNeedsLayout()
        guard let listViewModel = viewModel?.todoList else {
            return
        }
        listView?.layoutIfNeeded()
        listView?.viewModel = listViewModel
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    @objc func close() {
        dismiss(animated: true)
    }

    @objc func save() {
        viewModel?.save(todoList: listView?.viewModel, completion: { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        })
    }
}

extension TodoListViewController {
    func setupInput() {
        let input = TodoListView()
        view.addSubview(input)
        input.isDragEnabled = false
        input.isResizeEnable = false
        input.translatesAutoresizingMaskIntoConstraints = false
        input.leadingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.leadingAnchor,
            constant: 8).isActive = true
        input.topAnchor.constraint(
            equalTo: titleLabel.bottomAnchor,
            constant: -20).isActive = true
        input.trailingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.trailingAnchor,
            constant: 8).isActive = true
        input.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor,
            constant: 8).isActive = true

        input.textInput?.becomeFirstResponder()
        listView = input
    }
}
