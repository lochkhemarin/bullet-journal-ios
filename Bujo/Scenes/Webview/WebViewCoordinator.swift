//
//  WebViewCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 8/4/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class WebViewCoordinator: BaseCoordinator {
    let presentor: UIViewController
    let navigationController: UINavigationController
    private(set) var controller: WebViewController?
    let url: URL

    convenience init(presentor: UIViewController, urlString: String) throws {
        let initUrl = URL(string: urlString)
        try self.init(presentor: presentor, url: initUrl)
    }

    init(presentor: UIViewController, url: URL?) throws {
        guard let initUrl = url else {
            throw InitializeError.invalidURL
        }
        self.url = initUrl
        self.presentor = presentor
        navigationController = UINavigationController()
    }

    override func start() {
        let webController = WebViewController(nibName: "WebViewController", bundle: .main)
        controller = webController
        let initUrl = url
        navigationController.pushViewController(webController, animated: false)
        presentor.present(navigationController, animated: true) {
            webController.webview.load(URLRequest(url: initUrl))
        }
    }

    enum InitializeError: Error {
        case invalidURL
    }
}
