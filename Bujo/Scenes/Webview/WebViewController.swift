//
//  WebViewController.swift
//  Bujo
//
//  Created by Khemarin on 8/4/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    @IBOutlet weak var webview: WKWebView!
    weak var coordinator: WebViewCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
    }

    @objc func close() {
        navigationController?.dismiss(animated: true, completion: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if navigationController?.isBeingDismissed == true {
            coordinator?.end()
        }
    }
}
