//
//  CalendarTrackerCell.swift
//  Bujo
//
//  Created by Khemarin on 1/5/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class CalendarTrackerCell: UICollectionViewCell {
    private(set) var trackerView: CalendarTracker?

    override func awakeFromNib() {
        let trackerView = CalendarTracker(frame: bounds, month: Month(year: 1970, month: 1))
        addSubview(trackerView)
        self.trackerView = trackerView
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        trackerView?.frame = bounds
    }

    func setViewModel(_ viewModel: CalendarTrackerViewModel) {
        trackerView?.viewModel = viewModel
    }
}
