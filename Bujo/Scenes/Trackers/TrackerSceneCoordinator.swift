//
//  TrackerSceneCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 2/10/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class TrackerSceneCoordinator: BaseCoordinator {
    let tabBarController: UITabBarController
    let navigationController: UINavigationController
    weak var trackerController: TrackerViewController?
    let initialDate: Date

    init(tabBar: UITabBarController,
         navigationController: UINavigationController,
         initialDate date: Date) {
        self.navigationController = navigationController
        tabBarController = tabBar
        initialDate = date
    }

    override func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Screen.tracker.storyboardID)
        controller.tabBarItem.image = Asset.tracker.image
        controller.tabBarItem.title = "Tracker"

        if let trackerController = controller as? TrackerViewController {
            let trackerViewModel = TrackerScreenViewModel(date: initialDate)
            trackerController.viewModel = trackerViewModel
            trackerController.coordinator = self

            self.trackerController = trackerController
        }

        navigationController.pushViewController(controller, animated: false)
    }

    func openDatePicker(month: Month, sourceRect: CGRect?, sourceView: UIView?) {
        guard let viewController = trackerController else {
            return
        }
        let coordinator = MonthPickerCoordinator(presentor: viewController,
                                                 month: month,
                                                 sourceRect: sourceRect,
                                                 sourceView: sourceView,
                                                 delegate: viewController)
        addChildCoordinator(coordinator)
        coordinator.start()
    }

    func confirmDelete(okTap: (() -> Void)?) {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to delete?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            okTap?()
        }))
        alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        navigationController.present(alertController, animated: true, completion: nil)
    }
}
