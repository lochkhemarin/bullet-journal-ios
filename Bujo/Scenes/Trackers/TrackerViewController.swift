//
//  TrackerViewController.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class TrackerViewController: BaseViewController, TopHeaderViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    var viewModel: TrackerScreenViewModel?
    weak var coordinator: TrackerSceneCoordinator?
    weak var editingCalendar: CalendarTracker?

    @IBOutlet weak var headerView: TopHeaderView!
    @IBOutlet weak var emptyMessageWrapper: UIView!
    @IBOutlet weak var emptyButtonLabel: UILabel!
    @IBOutlet weak var emptyButtonCaption: UILabel!

    var shouldFocusOnLastTracker = false

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.allowsSelection = false
        headerView.screen = .tracker
        headerView.delegate = self

        emptyButtonLabel.font = theme.font.heading6
        emptyButtonLabel.textColor = theme.color.onBackground
        emptyButtonCaption.font = theme.font.caption
        emptyButtonCaption.textColor = theme.color.onBackground.withAlphaComponent(0.6)

        reloadTrackers()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)


        viewModel?.calendarTrackerListChange = { [weak self] in
            self?.setEmptyMessageHidden()
        }
    }

    private func reloadTrackers() {
        viewModel?.getCalendarTrackers(completion: { [weak self] in
            self?.getTrackersComplete()
        })
    }

    private func getTrackersComplete() {
        setEmptyMessageHidden()
        collectionView.reloadData()
    }

    private func setEmptyMessageHidden() {
        emptyMessageWrapper.isHidden = !(viewModel?.calendarTrackers.isEmpty ?? true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
        updateMonthToolbarTitle()
    }

    func updateMonthToolbarTitle() {
        headerView.month = AppSession.session.month
    }

    func removeCalendarTrackerFromUI(at index: Int) {
        let indexPath = IndexPath(item: index, section: 0)
        collectionView.performBatchUpdates({ [weak self] in
            self?.collectionView.deleteItems(at: [indexPath])
        }, completion: nil)
    }

    @IBAction func emptyAddButtonAction(_ sender: UIButton) {
        addNewTracker()
    }

    func addNewTracker() {
        editingCalendar?.titleInputText.resignFirstResponder()
        editingCalendar = nil
        viewModel?.addCalendarTracker(title: "Add title here",
                                      month: viewModel?.date.month,
                                      completion: { [weak self] in
                                        self?.shouldFocusOnLastTracker = true
                                        self?.collectionView.reloadData()
        })
    }
}

extension TrackerViewController: MonthPickerDelegate {
    func didTapDone(in picker: MonthPickerViewController, month: Month) {
        AppSession.session.month = month
        updateMonthToolbarTitle()
        if let date = month.firstDayDate {
            viewModel?.date = date
            reloadTrackers()
        }
    }

    func didTabCancel(in picker: MonthPickerViewController) {

    }
}

extension TrackerViewController: CalendarTrackerDelegate {
    func calendarTrackerDeleteDidTap(_ tracker: CalendarTracker) {
        editingCalendar?.titleInputText.resignFirstResponder()
        coordinator?.confirmDelete(okTap: { [weak self] in
            self?.viewModel?.deleteTracker(viewModel: tracker.viewModel, success: { [weak self] deletedIndex in
                self?.removeCalendarTrackerFromUI(at: deletedIndex)
            }, failed: {
                    //TODO: Show error
            })
        })
    }

    func calendarTrackerDayDidTap(_ tracker: CalendarTracker, at day: Int) {
        tracker.viewModel.toggleSelectAt(day: day)
        tracker.render()
        viewModel?.updateTracker(tracker.viewModel)
    }

    func calendarTrackerTitleChanged(_ tracker: CalendarTracker) {
        editingCalendar = nil
        viewModel?.updateTracker(tracker.viewModel)
    }

    func calendarTrackerTitleDidBeginEditing(_ tracker: CalendarTracker) {
        editingCalendar = tracker
    }
}

extension TrackerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarTrackerCell", for: indexPath)
        if let trackerCell = cell as? CalendarTrackerCell,
            let trackers = viewModel?.calendarTrackers,
            indexPath.item < trackers.count {
            trackerCell.trackerView?.delegate = self
            trackerCell.setViewModel(trackers[indexPath.item])
            setFocusIfNeeded(tracker: trackerCell, isLastIndex: indexPath.item == trackers.count - 1)
        }
        return cell
    }

    private func setFocusIfNeeded(tracker: CalendarTrackerCell, isLastIndex: Bool) {
        guard isLastIndex, shouldFocusOnLastTracker else {
            return
        }
        shouldFocusOnLastTracker = false
        tracker.trackerView?.titleInputText.becomeFirstResponder()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.calendarTrackers.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat
        let margin: CGFloat = 20
        var column: CGFloat
        if UI_USER_INTERFACE_IDIOM() == .pad {
            column = 2
        } else {
            column = 1
        }
        width = (collectionView.bounds.width - ((column + 1) * margin)) / column
        return CGSize(width: width, height: 270)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    // MARK: Header delegates
    func topHeaderDidTapNew(in view: TopHeaderView) {
        addNewTracker()
    }

    func topHeaderDidTapDate(in view: TopHeaderView) {
        let month = AppSession.session.month
        let rect = view.dateButton.bounds
        coordinator?.openDatePicker(month: month, sourceRect: rect, sourceView: view.dateButton)
    }
}

extension TrackerViewController {
    @objc func keyboardWillShow(_ notification: Notification) {
        guard isViewLoaded else {
            return
        }
        let userInfo = notification.userInfo
        guard let frameValue = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardFrame = frameValue.cgRectValue
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
    }

    @objc func keyboardWillHide() {
        collectionView.contentInset = .zero
    }
}
