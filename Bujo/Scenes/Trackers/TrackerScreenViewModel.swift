//
//  TrackerScreenViewModel.swift
//  Bujo
//
//  Created by Khemarin on 1/2/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class TrackerScreenViewModel {
    typealias VoidCallback = () -> Void
    var date: Date
    var calendarTrackers: [CalendarTrackerViewModel] = [] {
        didSet {
            calendarTrackerListChange?()
        }
    }

    var calendarTrackerListChange: (() -> Void)?

    init(date: Date) {
        self.date = date
    }

    func getCalendarTrackers(completion: VoidCallback) {
        DataManager.shared.localStorage.getCalendarTracker(from: date) { results in
            self.calendarTrackers = results.compactMap { CalendarTrackerViewModel(model: $0) }
            completion()
        }
    }

    func addCalendarTracker(title: String, month: Month? = nil, completion: VoidCallback? = nil) {
        let saveToMonth = month ?? date.month
        let viewModel = CalendarTrackerViewModel(month: saveToMonth)
        viewModel.title = title
        DataManager.shared.localStorage.saveCalendarTracker(viewModel: viewModel) { [weak self] response in
            self?.handleAddSuccess(response: response, viewModel: viewModel, completion: completion)
        }
    }

    private func handleAddSuccess(response: LocalStorage.SaveResponse,
                                  viewModel: CalendarTrackerViewModel,
                                  completion: VoidCallback?) {
        switch response {
        case .success(id: let identifier):
            viewModel.identifier = identifier
            calendarTrackers.append(viewModel)
        case .failed(error: _):
            break
        }
        completion?()
    }

    func updateTracker(_ viewModel: CalendarTrackerViewModel) {
        DataManager.shared.localStorage.saveCalendarTracker(viewModel: viewModel, completion: nil)
    }

    func deleteTracker(viewModel: CalendarTrackerViewModel, success: ((Int) -> Void)?, failed: VoidCallback?) {
        DataManager.shared.localStorage.deleteTracker(viewModel: viewModel) { [weak self] response in
            self?.handleDeleteCompleted(response: response, success: success, failed: failed)
        }
    }

    func handleDeleteCompleted(response: LocalStorage.SaveResponse,
                               success: ((Int) -> Void)?,
                               failed: VoidCallback?) {
        switch response {
        case .success(id: let id):
            if let deletedIndex = calendarTrackers.firstIndex(where: { $0.identifier == id }) {
                calendarTrackers.remove(at: deletedIndex)
                success?(deletedIndex)
            } else {
                failed?()
            }
        case .failed(error: _):
            failed?()
        }
    }
}
