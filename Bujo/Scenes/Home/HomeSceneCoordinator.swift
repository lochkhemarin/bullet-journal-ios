//
//  HomeSceneCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 2/11/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class HomeSceneCoordinator: BaseCoordinator {
    let tabBarController: UITabBarController
    let navigationController: UINavigationController
    weak var homeViewController: HomeViewController?
    let initialDate: Date

    init(tabBar: UITabBarController,
         navigationController: UINavigationController,
         initialDate date: Date) {
        self.navigationController = navigationController
        tabBarController = tabBar
        initialDate = date
    }

    override func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Screen.home.storyboardID)
        controller.tabBarItem.image = Asset.home.image
        controller.tabBarItem.title = "Home"

        if let homeViewController = controller as? HomeViewController {
            let homeViewModel = HomeViewModel(date: initialDate)
            homeViewController.viewModel = homeViewModel
            homeViewController.coordinator = self
            self.homeViewController = homeViewController
        }

        navigationController.pushViewController(controller, animated: false)
    }

    func openDatePicker(month: Month, sourceRect: CGRect?, sourceView: UIView?) {
        guard let viewController = homeViewController else {
            return
        }

        let coordinator = MonthPickerCoordinator(presentor: viewController,
                                                 month: month,
                                                 sourceRect: sourceRect,
                                                 sourceView: sourceView,
                                                 delegate: viewController)
        addChildCoordinator(coordinator)
        coordinator.start()
    }

    func openTodoListScreen(date: Date) {
        guard let presentor = homeViewController else {
            return
        }
        let todoListController = TodoListViewController()
        let coordinator = TodoListCoordinator(presentor: presentor,
                                              controller: todoListController,
                                              date: date)
        addChildCoordinator(coordinator)
        coordinator.start()
    }

    func closeTodoList() {
        guard let coordinator = childCoordinator.filter({ child in
            return child.value is TodoListCoordinator
        }).first?.value as? TodoListCoordinator else {
            return
        }
        coordinator.end()
    }
}
