//
//  HomeViewModel.swift
//  Bujo
//
//  Created by Khemarin on 11/24/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Foundation
import UIKit

class HomeViewModel {
    static let defaultListType = TodoListViewModel.ListType.none

    var selectedDate = Date()

    var month = Month(year: 2018, month: 10) {
        didSet {
            calendar = CalendarGrid(baseMonth: month)
        }
    }

    private var list: [TodoList] = []

    var calendar: CalendarGrid

    init(date: Date) {
        self.selectedDate = date
        self.month = date.month
        calendar = CalendarGrid(baseMonth: month)
    }

    func weekdaySymbols(forUserInterfaceIdiom idiom: UIUserInterfaceIdiom) -> [String] {
        switch idiom {
        case .phone, .unspecified:
            calendar.useShortWeekday = true
        default:
            calendar.useShortWeekday = false
        }
        return calendar.weekdaySymbols
    }

    func saveTodoList(_ list: TodoListViewModel, completion: @escaping () -> Void) {
        if (list.text ?? "").isEmpty {
            DataManager.shared.localStorage.deleteList(list) { _ in
                completion()
            }
        } else {
            DataManager.shared.localStorage.saveList(list, toMonthOfDate: selectedDate) { _ in
                completion()
            }
        }
    }

    func newTodoList(frame: CGRect, completion: @escaping () -> Void) {
        let viewModel = TodoListViewModel()
        viewModel.frame = frame
        viewModel.titleHeader = "Add title here"
        DataManager.shared.localStorage.saveList(viewModel, toMonthOfDate: selectedDate) { _ in
            completion()
        }
    }

    func getTodoList(forMonth month: Month, completion: @escaping () -> Void) {
        DataManager.shared.localStorage.getTodoListFromMonth(of: month.firstDayDate ?? selectedDate) { list in
            self.list = list
            completion()
        }
    }

    func countListTypesForSelectedMonth() -> [Int: [TodoListViewModel.ListType]] {
        var result: [Int: [TodoListViewModel.ListType]] = [:]
        for dailyList in list {
            let listViewModel = LocalStorage.createTodoViewModel(from: dailyList, defaultType: HomeViewModel.defaultListType)
            let types = listViewModel.paragraphTypes.compactMap { $0.type }
            let day = Calendar.current.component(.day, from: dailyList.date)
            result[day] = types
        }
        return result
    }
}
