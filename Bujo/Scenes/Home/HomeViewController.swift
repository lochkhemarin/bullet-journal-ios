//
//  HomeViewController.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    @IBOutlet weak var headerView: TopHeaderView!
    var viewModel: HomeViewModel?

    weak var coordinator: HomeSceneCoordinator?
    var bigCalendarView: BigCalendarView?
    var monthView: MonthHeaderView?

    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.screen = .home
        headerView.delegate = self
        initUI()
        renderCalendarIndicators()
    }

    func renderCalendarIndicators() {
        guard let viewModel = viewModel else {
            return
        }
        viewModel.getTodoList(forMonth: viewModel.month) { [weak self] in
            let indicators = viewModel.countListTypesForSelectedMonth()
            self?.bigCalendarView?.indicators = indicators
        }
    }

    func initUI() {
        let origin: CGFloat = 20
        let width = view.bounds.width - (2 * origin)
        let monthView = MonthHeaderView(frame: CGRect(x: origin,
                                                      y: headerView.frame.maxY,
                                                      width: width,
                                                      height: 80),
                                        month: 1)
        view.addSubview(monthView)
        self.monthView = monthView

        let calendarView = BigCalendarView(frame: CGRect(x: origin,
                                                         y: monthView.frame.maxY,
                                                         width: width,
                                                         height: calendarHeight()))
        calendarView.month = viewModel?.month ?? Date().month
        calendarView.weekdaySymbols = viewModel?.weekdaySymbols(forUserInterfaceIdiom: UI_USER_INTERFACE_IDIOM()) ?? []
        view.addSubview(calendarView)
        calendarView.delegate = self
        calendarView.selectedDay = Calendar.current.component(.day, from: Date())
        bigCalendarView = calendarView
    }

    fileprivate func calendarHeight() -> CGFloat {
        let cellHeight = (view.bounds.width - 40) / 7
        let height = CGFloat((viewModel?.calendar.grid?.count ?? 0) + 1) * cellHeight
        return height + 40
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        updateMonthInView()
    }

    func updateMonthInView() {
        let selectedMonth = AppSession.session.month
        headerView.month = selectedMonth
        monthView?.month = selectedMonth.month

        if let calendar = bigCalendarView {
            let frame = calendar.frame
            calendar.frame = CGRect(origin: frame.origin,
                                    size: CGSize(width: frame.size.width, height: calendarHeight()))
            calendar.month = selectedMonth
            renderCalendarIndicators()
        }
    }
}

// MARK: - Header delegates
extension HomeViewController: TopHeaderViewDelegate {
    func topHeaderDidTapNew(in view: TopHeaderView) {
        guard let selectedDate = viewModel?.selectedDate else {
            return
        }
        coordinator?.openTodoListScreen(date: selectedDate)
    }

    func topHeaderDidTapDate(in view: TopHeaderView) {
        let month = AppSession.session.month
        let rect = view.dateButton.bounds
        coordinator?.openDatePicker(month: month, sourceRect: rect, sourceView: view.dateButton)
    } 
}

// MARK: - MonthPickerDelegate
extension HomeViewController: MonthPickerDelegate {
    func didTapDone(in picker: MonthPickerViewController, month: Month) {
        AppSession.session.month = month
        viewModel?.month = month
        bigCalendarView?.selectedDay = 0
        viewModel?.selectedDate = Date()
        updateMonthInView()
    }

    func didTabCancel(in picker: MonthPickerViewController) {

    }
}

// MARK: - BigCalendarViewDelegate
extension HomeViewController: BigCalendarViewDelegate {
    func dateClick(in calendarView: BigCalendarView, day: Int) {
        guard let month = viewModel?.month else {
            return
        }
        var component = DateComponents()
        component.year = month.year
        component.month = month.month
        component.day = day
        if let date = Calendar.current.date(from: component) {
            viewModel?.selectedDate = date
            coordinator?.openTodoListScreen(date: date)
        }
    }
}

extension HomeViewController: InteractivePresentor, InteractiveControllerDelegate {
    func interactiveController(_ controller: InteractiveController, didFinishAnimatingToStep step: Int) {
        if step == 0 {
            coordinator?.closeTodoList()
            renderCalendarIndicators()
        }
    }
}
