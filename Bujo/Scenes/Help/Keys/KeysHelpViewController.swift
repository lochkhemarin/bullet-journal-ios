//
//  KeysHelpViewController.swift
//  Bujo
//
//  Created by Khemarin on 6/8/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit
import SafariServices

class KeysHelpViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var keyLabels: [UILabel]!

    var activityView = UIActivityIndicatorView(style: .gray)

    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!

    let viewModel = AboutViewModel()
    var coordinator: HelpCoordinator?

    let theme = BujoTheme.shared

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Help"
        navigationController?.navigationBar.isHidden = true

        titleLabel.font = theme.font.heading5
        titleLabel.textColor = theme.color.onBackground
        for keyLabel in keyLabels {
            keyLabel.font = theme.font.body1
            keyLabel.textColor = theme.color.onBackground
        }

        appNameLabel.font = theme.font.heading3
        appNameLabel.textColor = theme.color.onBackground
        appNameLabel.text = viewModel.productName

        versionLabel.font = theme.font.subtitle2
        versionLabel.textColor = theme.color.onBackground
        versionLabel.text = viewModel.version

        termsButton.titleLabel?.font = theme.font.body1
        termsButton.setTitleColor(theme.color.secondary, for: .normal)
        privacyButton.titleLabel?.font = theme.font.body1
        privacyButton.setTitleColor(theme.color.secondary, for: .normal)
    }

    @IBAction func privacyAction(_ sender: Any) {
        coordinator?.openPrivacy()
    }

    @IBAction func termConditionAction(_ sender: UIButton) {
        coordinator?.openTerms()
    }
}
