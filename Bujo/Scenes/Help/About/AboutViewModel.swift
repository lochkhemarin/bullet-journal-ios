//
//  AboutViewModel.swift
//  Bujo
//
//  Created by Khemarin on 6/16/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import Foundation

class AboutViewModel {
    let productName: String
    let title: String
    let version: String

    enum HelpUrl {
        static let scheme = "https://"
        static let base = "bujo-d9d7e.web.app"
        static let privacy = "\(HelpUrl.scheme)\(HelpUrl.base)/privacy_policy.html"
        static let term = "\(HelpUrl.scheme)\(HelpUrl.base)/terms_condition.html"
    }

    init() {
        productName = "Fokus"
        title = "About"
        version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0.0"
    }

    func privacyUrl() -> URL? {
        return URL(string: HelpUrl.privacy)
    }

    func termsUrl() -> URL? {
        return URL(string: HelpUrl.term)
    }
}
