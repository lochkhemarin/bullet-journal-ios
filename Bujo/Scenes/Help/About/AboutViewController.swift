//
//  AboutViewController.swift
//  Bujo
//
//  Created by Khemarin on 6/16/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    let viewModel = AboutViewModel()

    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var libraryButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!

    let theme = BujoTheme.shared

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title

        appNameLabel.font = Font.boldScriptFont(ofSize: 35)
        appNameLabel.textColor = theme.color.onBackground
        appNameLabel.text = viewModel.productName

        versionLabel.font = Font.scriptFont(ofSize: 20)
        versionLabel.textColor = theme.color.onBackground
        versionLabel.text = viewModel.version

        libraryButton.titleLabel?.font = theme.font.button
        libraryButton.setTitleColor(theme.color.secondary, for: .normal)
        privacyButton.titleLabel?.font = theme.font.button
        privacyButton.setTitleColor(theme.color.secondary, for: .normal)
    }
}
