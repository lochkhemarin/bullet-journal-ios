//
//  HelpViewController.swift
//  Bujo
//
//  Created by Khemarin on 6/8/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    var viewModel = HelpViewModel()
    weak var coordinator: HelpCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Help"
        tableView.tableFooterView = UIView()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTableViewCell", for: indexPath)
        cell.textLabel?.textAlignment = .center
        if indexPath.row < viewModel.rows.count {
            cell.textLabel?.text = viewModel.rows[indexPath.row]
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rows.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            coordinator?.openKeyView()
        case 1:
            coordinator?.openAboutView()
        default:
            break
        }
    }
}
