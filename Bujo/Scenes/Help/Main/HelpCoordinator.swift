//
//  HelpCoordinator.swift
//  Bujo
//
//  Created by Khemarin on 6/11/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class HelpCoordinator: BaseCoordinator {
    let navigationController: UINavigationController
    weak var viewModel: AboutViewModel?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    override func start() {
        let controller = KeysHelpViewController(nibName: "KeysHelpViewController", bundle: nil)
        controller.tabBarItem.image = Asset.i.image
        controller.tabBarItem.title = "Help"

        controller.coordinator = self
        viewModel = controller.viewModel
        navigationController.pushViewController(controller, animated: false)
    }

    func openKeyView() {
        let controller = KeysHelpViewController(nibName: "KeysHelpViewController", bundle: nil)
        navigationController.pushViewController(controller, animated: true)
    }

    func openAboutView() {
        let controller = AboutViewController(nibName: "AboutViewController", bundle: nil)
        navigationController.pushViewController(controller, animated: true)
    }

    func openPrivacy() {
        openWebView(url: viewModel?.privacyUrl())
    }

    func openTerms() {
        openWebView(url: viewModel?.termsUrl())
    }

    private func openWebView(url: URL?) {
        guard let webCoordinator = try? WebViewCoordinator(presentor: navigationController, url: url) else {
            return
        }
        addChildCoordinator(webCoordinator)
        webCoordinator.start()
    }
}
