//
//  HelpTableViewCell.swift
//  Bujo
//
//  Created by Khemarin on 6/11/19.
//  Copyright © 2019 Vemean. All rights reserved.
//

import UIKit

class HelpTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        textLabel?.font = Font.scriptFont(ofSize: 18)
    }
}
