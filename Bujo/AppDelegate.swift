//
//  AppDelegate.swift
//  Bujo
//
//  Created by Khemarin on 9/30/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import UIKit
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var appCoordinator: AppCoordinator?
    var appSession: AppSession = AppSession()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configLog()
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])

        let initialWindow = UIWindow(frame: UIScreen.main.bounds)
        window = initialWindow
        appCoordinator = AppCoordinator(window: initialWindow)
        appCoordinator?.start()

        _ = DataManager.shared.localStorage

        return true
    }

    func configLog() {
    }
}
