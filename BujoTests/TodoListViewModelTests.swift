//
//  TodoListViewModelTests.swift
//  BujoTests
//
//  Created by Khemarin on 12/4/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import XCTest
@testable import Bujo

class TodoListViewModelTests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /// Given: we have some paragraphs
    /// When: we create list without providing list type
    /// Then: we should get list of pargraph type .none
    func testParagraphsWithNoDefaultType() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\n"
        let list = viewModel.paragraphTypes(for: paragraph, types: [])
        guard list.count == 3 else {
            XCTFail("Paragraphs count should be 3")
            return
        }
        XCTAssertEqual(list.first?.range.lowerBound, 0)
        XCTAssertEqual(list.first?.range.upperBound, 5)
        XCTAssertEqual(list.first?.type, TodoListViewModel.ListType.none)

        XCTAssertEqual(list[1].range.lowerBound, 6)
        XCTAssertEqual(list[1].range.upperBound, 11)
        XCTAssertEqual(list[1].type, TodoListViewModel.ListType.none)

        XCTAssertEqual(list[2].range.lowerBound, 12)
        XCTAssertEqual(list[2].range.upperBound, 12)
        XCTAssertEqual(list[2].type, TodoListViewModel.ListType.none)
    }

    /// Given: we have some paragraphs
    /// When: we create list with providing list type less than total paragraphs
    /// Then: we should get list of pargraph type .none
    func testParagraphsWithDefaultTypeLessThanOrEqualTotalParagraph() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\n"
        var defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: nil)
        ]
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        guard list.count == 3 else {
            XCTFail("Paragraphs count should be 3")
            return
        }
        XCTAssertEqual(list.first?.range.lowerBound, 0)
        XCTAssertEqual(list.first?.range.upperBound, 5)
        XCTAssertEqual(list.first?.type, defaultTypes.first)

        XCTAssertEqual(list[1].range.lowerBound, 6)
        XCTAssertEqual(list[1].range.upperBound, 11)
        XCTAssertEqual(list[1].type, TodoListViewModel.ListType.none)

        defaultTypes.append(.task(state: .normal, signifier: .important))
        let list2 = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        XCTAssertEqual(list2[1].range.lowerBound, 6)
        XCTAssertEqual(list2[1].range.upperBound, 11)
        XCTAssertEqual(list2[1].type, defaultTypes.last)
    }

    /// Given: we have some paragraphs
    /// When: we create list with providing list type less than total paragraphs
    /// Then: we should get list of pargraph type .none
    func testParagraphsWithDefaultTypeMoreThanTotalParagraph() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld"
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: .important),
            TodoListViewModel.ListType.note(state: .normal, signifier: nil)
        ]
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        XCTAssertEqual(list.count, 2)
        XCTAssertEqual(list.first?.range.lowerBound, 0)
        XCTAssertEqual(list.first?.range.upperBound, 5)
        XCTAssertEqual(list.first?.type, defaultTypes.first)

        XCTAssertEqual(list.last?.range.lowerBound, 6)
        XCTAssertEqual(list.last?.range.upperBound, 11)
        XCTAssertEqual(list.last?.type, defaultTypes[1])
    }

    /// Given: We have a paragraph with list type
    /// When: Adding new line at the beginning
    /// Then: We should have a type .none empty paragraph
    func testReplacingNewLineAtBeginning() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph"
        //0-5,6-11,12-25
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: .important),
            TodoListViewModel.ListType.note(state: .normal, signifier: nil)
        ]
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        let newList = try? viewModel.replaceTextInRange(NSRange(location: 0, length: 0),
                                                   with: "\n",
                                                   paragraphText: paragraph,
                                                   types: defaultTypes)
        XCTAssertEqual(newList?.count, list.count + 1, "There should be new paragraph inserted")
        XCTAssertEqual(newList?.first?.type,
                       TodoListViewModel.ListType.none,
                       "New inserted paragraph should have type none")
    }

    /// Given: We have a paragraph with list type
    /// When: Adding new line at the end of the list
    /// Then: We should have new paragraph
    func testReplacingNewLineAtEnd() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph"
        //0-5,6-11,12-25
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: .important),
            TodoListViewModel.ListType.note(state: .normal, signifier: nil)
        ]
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        let endRange = NSRange(location: paragraph.utf16.count, length: 0)
        let newList = try? viewModel.replaceTextInRange(endRange,
                                                        with: "\n",
                                                        paragraphText: paragraph,
                                                        types: defaultTypes)
        XCTAssertEqual(newList?.count, list.count + 1, "There should be new paragraph inserted")
    }

    /// Given: We have a paragraph with list type
    /// When: Adding new line at middle of the list
    /// Then: We should correct paragraphs and types
    func testReplacingNewLine() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph"
        //{0,5},{6,5},{12,14}
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: .important),
            TodoListViewModel.ListType.note(state: .normal, signifier: nil)
        ]
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        let range = NSRange(location: 8, length: 0)
        let newList = try? viewModel.replaceTextInRange(range,
                                                        with: "\n",
                                                        paragraphText: paragraph,
                                                        types: defaultTypes)
        //"Hello\nWo\nrld\nTest paragraph"
        //{0,5},{6,2},{9,3},{13,14}
        XCTAssertEqual(newList?.count, list.count + 1, "There should be new paragraph inserted")
        XCTAssertEqual(newList?[0].type, defaultTypes[0])
        XCTAssertEqual(newList?[1].type, defaultTypes[1])
        XCTAssertEqual(newList?[2].type, TodoListViewModel.ListType.none)
        XCTAssertEqual(newList?[3].type, defaultTypes[2])

        assertSameRange(newList?[0].range, start: 0, length: 5, message: "")
        assertSameRange(newList?[1].range, start: 6, length: 2, message: "")
        assertSameRange(newList?[2].range, start: 9, length: 3, message: "")
        assertSameRange(newList?[3].range, start: 13, length: 14, message: "")
    }

    /// Given: Cursor is at the paragraph range
    /// When: user want to remove a character
    /// Then: Total paragraph should not change but only range of modified paragraph
    func testRemoveOneCharacterInMiddleOfParagraph() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph"
        //0-5,6-11,12-24: {0,5}, {6,5}, {12,14}
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: .important),
            TodoListViewModel.ListType.note(state: .normal, signifier: nil)
        ]
        let editRange = NSRange(location: 7, length: 1)
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        let newList = try? viewModel.replaceTextInRange(editRange,
                                                        with: "",
                                                        paragraphText: paragraph,
                                                        types: defaultTypes)
        XCTAssertEqual(newList?.count, list.count, "There should be no change in paragraph count")
        for index in 0..<list.count {
            XCTAssertEqual(newList?[index].type, defaultTypes[index], "Type of element \(index) should remain")
        }
        assertSameRange(newList?[0].range, start: 0, length: 5, message: "Paragraph 1 should not be changed")
        assertSameRange(newList?[1].range, start: 6, length: 4, message: "Paragraph 2 should be changed")
        assertSameRange(newList?[2].range, start: 11, length: 14, message: "Paragraph 3 index should be shift")
    }

    /// Given: Cursor is at the begining of the paragraph
    /// When: User click delete
    /// Then: Paragraphs and types should be correct
    func testRemoveLinebreak() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph\nabc"
        //{0,5}, {6,5}, {12,14}, {27,3}
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.event(state: .normal, signifier: .inspiration),
            TodoListViewModel.ListType.task(state: .normal, signifier: .important),
            TodoListViewModel.ListType.note(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: nil)
        ]
        let editRange = NSRange(location: 5, length: 1)
        let list = viewModel.paragraphTypes(for: paragraph, types: defaultTypes)
        let newList = try? viewModel.replaceTextInRange(editRange,
                                                        with: "",
                                                        paragraphText: paragraph,
                                                        types: defaultTypes)
        XCTAssertEqual(newList?.count, list.count - 1, "Paragraph count should be changed")
        assertSameRange(newList?[0].range, start: 0, length: 10, message: "Paragraph 1 and 2 should be merged")
        assertSameRange(newList?[1].range, start: 11, length: 14, message: "Paragraph 3 should shift index to 2")
        assertSameRange(newList?[2].range, start: 26, length: 3, message: "Paragraph 4 should shift index to 3")

        XCTAssertEqual(newList?[0].type, defaultTypes[0])
        XCTAssertEqual(newList?[1].type, defaultTypes[2])
        XCTAssertEqual(newList?[2].type, defaultTypes[3])
    }

    /// Given: User select a range containing linebreaks
    /// When: Replacing with new text
    /// Then: Paragraphs and types should be correct
    func testReplacingInTextWhichLeadToLessParagraph() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph\nabc"
        //{0,5}, {6,5}, {12,14}, {27,3}
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.task(state: .normal, signifier: .inspiration),
            TodoListViewModel.ListType.note(state: .normal, signifier: .important),
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: nil)
        ]
        let selectRange = NSRange(location: 3, length: 12)
        let replacingText = "new\ntext"
        // New pagraphs: Hello\nWorld\nTest paragraph\nabc
        //  {0,6},{7,15},{23,3}
        let newList = try? viewModel.replaceTextInRange(selectRange,
                                                        with: replacingText,
                                                        paragraphText: paragraph,
                                                        types: defaultTypes)
        guard let list = newList else {
            XCTFail("New list should be valid.")
            return
        }
        XCTAssertEqual(list.count, 3)
        assertSameRange(list[0].range, start: 0, length: 6, message: "")
        assertSameRange(list[1].range, start: 7, length: 15, message: "")
        assertSameRange(list[2].range, start: 23, length: 3, message: "")
        XCTAssertEqual(list[0].type, defaultTypes[0])
        XCTAssertEqual(list[1].type, TodoListViewModel.ListType.none)
        XCTAssertEqual(list[2].type, defaultTypes[3])
    }

    /// Given: User select a range containing linebreaks
    /// When: Replacing with new text
    /// Then: Paragraphs and types should be correct
    func testReplacingInTextWhichLeadToMoreParagraph() {
        let viewModel = TodoListViewModel()

        let paragraph = "Hello\nWorld\nTest paragraph\nabc"
        //{0,5}, {6,5}, {12,14}, {27,3}
        let defaultTypes: [TodoListViewModel.ListType] = [
            TodoListViewModel.ListType.task(state: .normal, signifier: .inspiration),
            TodoListViewModel.ListType.note(state: .normal, signifier: .important),
            TodoListViewModel.ListType.event(state: .normal, signifier: nil),
            TodoListViewModel.ListType.task(state: .normal, signifier: nil)
        ]
        let selectRange = NSRange(location: 3, length: 12)
        let replacingText = "new\ntext\nred\nyellow"
        // New pagraphs: Helnew\ntext\nred\nyellowt paragraph\nabc
        //  {0,6},{7,4},{12,3},{16,17},{34,3}
        let newList = try? viewModel.replaceTextInRange(selectRange,
                                                        with: replacingText,
                                                        paragraphText: paragraph,
                                                        types: defaultTypes)
        guard let list = newList else {
            XCTFail("New list should be valid.")
            return
        }
        XCTAssertEqual(list.count, 5)
        assertSameRange(list[0].range, start: 0, length: 6, message: "")
        assertSameRange(list[1].range, start: 7, length: 4, message: "")
        assertSameRange(list[2].range, start: 12, length: 3, message: "")
        assertSameRange(list[3].range, start: 16, length: 17, message: "")
        assertSameRange(list[4].range, start: 34, length: 3, message: "")
        XCTAssertEqual(list[0].type, defaultTypes[0])
        XCTAssertEqual(list[1].type, TodoListViewModel.ListType.none)
        XCTAssertEqual(list[2].type, TodoListViewModel.ListType.none)
        XCTAssertEqual(list[3].type, TodoListViewModel.ListType.none)
        XCTAssertEqual(list[4].type, defaultTypes[3])
    }

    func assertSameRange(_ range: NSRange?,
                         start: Int,
                         length: Int,
                         message: String,
                         line: Int = #line) {
        guard let range = range else {
            XCTFail("Nil is not equal to a range")
            return
        }
        let isSameRange = range.lowerBound == start && range.length == length
        let compareText = "{\(start), \(length)}"
        XCTAssertTrue(isSameRange, "\(#file):\(line) - \(range) and \(compareText) \(message)")
    }
}
