//
//  CalendarGridTests.swift
//  BujoTests
//
//  Created by Khemarin on 10/14/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import XCTest
@testable import Bujo

class CalendarGridTests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testWeekdays() {
        let weekdays = Calendar.current.weekdaySymbols
        let month = Month(year: 2018, month: 10)
        XCTAssertEqual(CalendarGrid(baseMonth: month).weekdaySymbols, weekdays)
    }

    func testGrid1() {
        let month = Month(year: 2018, month: 10)
        let expectedResult = [
            [0, 1, 2, 3, 4, 5, 6],
            [7, 8, 9, 10, 11, 12, 13],
            [14, 15, 16, 17, 18, 19, 20],
            [21, 22, 23, 24, 25, 26, 27],
            [28, 29, 30, 31, 0, 0, 0]
        ]
        let grid = CalendarGrid(baseMonth: month)
        XCTAssertNotNil(grid.grid)
        guard let gridResult = grid.grid else {
            return
        }
        XCTAssertEqual(gridResult, expectedResult)
    }

    func testGrid2() {
        let month = Month(year: 2008, month: 2)
        let expectedResult = [
            [0, 0, 0, 0, 0, 1, 2],
            [3, 4, 5, 6, 7, 8, 9],
            [10, 11, 12, 13, 14, 15, 16],
            [17, 18, 19, 20, 21, 22, 23],
            [24, 25, 26, 27, 28, 29, 0]
        ]
        let grid = CalendarGrid(baseMonth: month)
        XCTAssertNotNil(grid.grid)
        guard let gridResult = grid.grid else {
            return
        }
        XCTAssertEqual(gridResult, expectedResult)
    }

    func testGrid3() {
        let month = Month(year: 2015, month: 2)
        let expectedResult = [
            [1, 2, 3, 4, 5, 6, 7],
            [8, 9, 10, 11, 12, 13, 14],
            [15, 16, 17, 18, 19, 20, 21],
            [22, 23, 24, 25, 26, 27, 28]
            ]
        let grid = CalendarGrid(baseMonth: month)
        XCTAssertNotNil(grid.grid)
        guard let gridResult = grid.grid else {
            return
        }
        XCTAssertEqual(gridResult, expectedResult)
    }

    func testGrid4ShiftStartWeekday() {
        let month = Month(year: 2015, month: 2)
        let expectedResult1 = [
          // 2, 3, 4, 5, 6, 7, 1
            [0, 0, 0, 0, 0, 0, 1],
            [2, 3, 4, 5, 6, 7, 8],
            [9, 10, 11, 12, 13, 14, 15],
            [16, 17, 18, 19, 20, 21, 22],
            [23, 24, 25, 26, 27, 28, 0]
        ]
        let grid = CalendarGrid(baseMonth: month)
        grid.calendar.firstWeekday = 2 // start monday
        XCTAssertNotNil(grid.grid)
        guard let gridResult = grid.grid else {
            return
        }
        XCTAssertEqual(gridResult, expectedResult1)

        // Shift another day
        let expectedResult2 = [
            // 3, 4, 5, 6, 7, 1, 2
            [0, 0, 0, 0, 0, 1, 2],
            [3, 4, 5, 6, 7, 8, 9],
            [10, 11, 12, 13, 14, 15, 16],
            [17, 18, 19, 20, 21, 22, 23],
            [24, 25, 26, 27, 28, 0, 0]
        ]
        let grid2 = CalendarGrid(baseMonth: month)
        grid2.calendar.firstWeekday = 3 // start tuesday
        XCTAssertNotNil(grid2.grid)
        guard let gridResult2 = grid2.grid else {
            return
        }
        XCTAssertEqual(gridResult2, expectedResult2)
    }

    func testInvalidDate() {
        let month = Month(year: 2015, month: 22)
        let grid = CalendarGrid(baseMonth: month)
        XCTAssertNil(grid.grid)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
