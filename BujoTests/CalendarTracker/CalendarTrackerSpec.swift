//
//  CalendarTrackerSpec.swift
//  BujoTests
//
//  Created by Khemarin on 12/18/18.
//  Copyright © 2018 Vemean. All rights reserved.
//

import Quick
import Nimble
@testable import Bujo

class CalendarTrackerSpec: QuickSpec {
    override func spec() {
        describe("Calendar tracker") {
            var calendarTracker: CalendarTracker!
            beforeEach {
                let frame = CGRect(origin: .zero, size: CGSize(width: 140, height: 140))
                let month = Month(year: 2018, month: 12)
                calendarTracker = CalendarTracker(frame: frame, month: month)
            }

            context("When initialize") {
                it("Should update day list") {
                    expect(calendarTracker.viewModel.calendar.days).to(equal(31))
                }
            }

            context("When set month") {
                it("Should update day list") {
                    calendarTracker.month = Month(year: 2018, month: 11)
                    expect(calendarTracker.viewModel.calendar.days).to(equal(30))
                }
            }

            context("When set title") {
                it("Title label should be updated") {
                    calendarTracker.viewModel.title = "ABC"
                    expect(calendarTracker.titleInputText.text).to(equal("ABC"))

                    calendarTracker.viewModel.title = "1234"
                    expect(calendarTracker.titleInputText.text).to(equal("1234"))
                }
            }
        }

        describe("Calendar Tracker View Model") {
            var viewModel: CalendarTrackerViewModel!
            beforeEach {
                viewModel = CalendarTrackerViewModel(month: Month(year: 2018, month: 12))
            }
            context("When onitialize") {
                it("calendar should use short weekday") {
                    expect(viewModel.calendar.useShortWeekday).to(beTrue())
                }
            }

            context("Toggle select day") {
                context("Day < 0") {
                    it("should do nothing if is less than zero") {
                        viewModel.toggleSelectAt(day: -1)
                        expect(viewModel.selectedDays).to(beEmpty())
                    }
                }
                context("day is more than max date") {
                    it("should do nothing if is less than zero") {
                        viewModel.toggleSelectAt(day: 32)
                        expect(viewModel.selectedDays).to(beEmpty())
                    }
                }

                context("On toggle") {
                    it("Should add to list") {
                        viewModel.toggleSelectAt(day: 2)
                        expect(viewModel.selectedDays).to(contain(2))
                    }
                }

                context("Off toggle") {
                    it("Should remove from list") {
                        viewModel.selectedDays = [2]
                        viewModel.toggleSelectAt(day: 2)
                        expect(viewModel.selectedDays).notTo(contain(2))
                    }
                }
            }
        }
    }
}
